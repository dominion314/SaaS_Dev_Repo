terraform {
  source = "../../modules//eks"
}

locals {
  # Automatically load environment-level variables
  environment_vars             = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  env                          = local.environment_vars.locals.environment
  main_nodegroup_instance_type = local.environment_vars.locals.main_nodegroup_instance_type
}

inputs = {
  environment_name              = dependency.parameters.outputs.environment_name
  eks_security_group_id         = dependency.parameters.outputs.eks_security_group_id
  eks_control_security_group_id = dependency.parameters.outputs.eks_control_security_group_id
  private_subnets               = dependency.parameters.outputs.private_subnets
  vpc_id                        = dependency.parameters.outputs.vpc_id
  kms_key_id                    = dependency.parameters.outputs.key_alias_arn
  customer_name                 = dependency.parameters.outputs.customer_name
  main_nodegroup_instance_type  = local.main_nodegroup_instance_type
}

dependency "parameters" {
  config_path = "../parameters"
  mock_outputs = {
    environment_name              = "temporary-dummy-id"
    eks_security_group_id         = "temporary-dummy-id"
    eks_control_security_group_id = "temporary-dummy-id"
    private_subnets               = "temporary-dummy-id"
    vpc_id                        = "temporary-dummy-id"
    key_alias_arn                 = "temporary-dummy-id"
    customer_name                 = "dummy"
  }
}


dependencies {
  paths = ["../parameters"]
}