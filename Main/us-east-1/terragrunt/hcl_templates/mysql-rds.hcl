terraform {
  source = "tfr://app.terraform.io/shieldfc/mysql-rds/aws//?version=0.1.0"

}

locals {
  environment_vars  = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  env               = local.environment_vars.locals.environment
  rds_instance_type = local.environment_vars.locals.rds_instance_type
}

inputs = {
  environment_name       = local.env
  hosted_zone_id         = dependency.parameters.outputs.private_hosted_zone_id
  private_domain_name    = dependency.parameters.outputs.private_domain_name
  data_subnets           = dependency.parameters.outputs.database_subnets
  rds_rds_multiaz        = false
  rds_instance_type      = local.rds_instance_type
  rds_security_group_ids = [dependency.parameters.outputs.data_security_group_id]
  rds_kms_key_arn        = dependency.parameters.outputs.key_alias_arn

  rds_instance_static_protection = true
  rds_skip_final_snapshot        = false
}

dependency "parameters" {
  config_path = "../parameters"
  mock_outputs = {
    private_domain_name    = "temporary-dummy"
    database_subnets       = ["temporary-dummy"]
    data_security_group_id = "temporary-dummy"
    key_alias_arn          = "temporary-dummy"
    private_hosted_zone_id = "temporary-dummy"
  }
}

dependencies {
  paths = ["../parameters", "../eks", "../vault"]
}
