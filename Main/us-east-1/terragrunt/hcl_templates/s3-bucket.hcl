terraform {
  source = "${local.base_source_url}?ref=v0.0.221"
}

locals {
  # Automatically load environment-level variables
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  env = local.environment_vars.locals.environment
  aws_region = local.environment_vars.locals.aws_region
  customer_name = local.environment_vars.locals.customer_name
  customer_id = local.environment_vars.locals.customer_id
  base_source_url = "git::git@bitbucket.org:shielddev/terraform-modules.git//data-sources/s3-shield-archive"

  lifecycle_policies = read_terragrunt_config("${dirname(find_in_parent_folders())}/hcl_templates/lifecycle_policies.hcl")
  lifecycle_delete_worm_1d = local.lifecycle_policies.locals.lifecycle_delete_worm_1d
  lifecycle_delete_worm_14d = local.lifecycle_policies.locals.lifecycle_delete_worm_14d
  #lifecycle_rule_1825d = local.lifecycle_policies.locals.lifecycle_delete_1825d
  s3_bucket_policy = local.lifecycle_policies.locals.archive_bucket_policy
  



}

inputs = {
  customer_name                 = local.customer_name
  customer_id                   = local.customer_id
  environment                   = dependency.parameters.outputs.environment_name
  archive_hostname              = "archive.${dependency.parameters.outputs.private_domain_name}"
  versioning                    = true
  lifecycle_rule                = local.lifecycle_delete_worm_14d
  bucket_default_retention      = 1
  bucket_name                   = "SFTP_BUCKET-${dependency.parameters.outputs.environment_name}"
  # archive_name                  = "${dependency.parameters.outputs.environment_name} AWS Archive"
  add_archvie_record            = false
  custom_kms_key_arn            = dependency.parameters.outputs.key_alias_arn
  s3_endpoint_id                = dependency.parameters.outputs.s3_aws_vpc_endpoint
  bucket_policy                 = local.s3_bucket_policy # Policy to restrict all access except from allowed ips
  vault_address                 = "https://vault.${dependency.parameters.outputs.private_domain_name}:443"
  bucket_default_retention_mode = "COMPLIANCE"      #should be changed in producation      
       
}

dependency "parameters" {
  config_path = "../../parameters"
  mock_outputs = {
    environment_name = "temporary-dummy"
    private_domain_name = "temporary-dummy"
    key_alias_arn = "temporary-dummy"
  }
}

dependencies {
  paths = ["../../parameters","../../vault"]
}

// Overall, this HCL file is configuring a Terraform module to create an S3 bucket with various settings and dependencies on other modules.

// The file is defining the source for the Terraform module, which is stored in a Git repository. It is also defining several local variables to be used throughout the code.

// The inputs section defines the inputs required for the Terraform module, such as the customer name and ID, the environment, and various settings related to archiving data to an S3 bucket.

// The dependency section specifies a dependency on another Terraform module called "parameters", which provides values for environment_name, private_domain_name, and key_alias_arn.

// Finally, the dependencies section specifies the paths for the "parameters" and "vault" Terraform modules that are being used.




