# terraform {
#   source = "tfr://app.terraform.io/shieldfc/s3-archive/aws//?version=0.1.0"
# }

terraform {
  source = "${local.base_source_url}?ref=v0.0.221"
}

locals {
  # Automatically load environment-level variables
  environment_vars          = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  env                       = local.environment_vars.locals.environment
  aws_region                = local.environment_vars.locals.aws_region
  customer_name             = local.environment_vars.locals.customer_name
  customer_id               = local.environment_vars.locals.customer_id
  lifecycle_policies        = read_terragrunt_config("${dirname(find_in_parent_folders())}/hcl_templates/lifecycle_policies.hcl")
  lifecycle_delete_worm_1095d  = local.lifecycle_policies.locals.lifecycle_delete_worm_1095d
  archive_bucket_policy     = local.lifecycle_policies.locals.archive_bucket_policy
  base_source_url = "git::git@bitbucket.org:shielddev/terraform-modules.git//data-sources/s3-shield-archive"
}

inputs = {
  customer_name            = local.customer_name
  customer_id              = local.customer_id
  environment              = dependency.parameters.outputs.environment_name
  archive_hostname         = "archive.${dependency.parameters.outputs.private_domain_name}"
  versioning               = true
  lifecycle_rule           = local.lifecycle_delete_worm_1095d
  bucket_default_retention = 1095
  bucket_name              = "archive-${dependency.parameters.outputs.environment_name}"
  archive_name             = "${dependency.parameters.outputs.environment_name}-policy-archive"
  add_archvie_record       = true
  custom_kms_key_arn       = dependency.parameters.outputs.key_alias_arn
  s3_endpoint_id           = dependency.parameters.outputs.s3_aws_vpc_endpoint
  bucket_policy            = local.archive_bucket_policy # Policy to restrict all access except from VPC s3 endpoint
  vault_address            = "https://vault.${dependency.parameters.outputs.private_domain_name}:443"
  bucket_default_retention_mode = "COMPLIANCE"      #should be changed in producation      

}

dependency "parameters" {
  config_path = "../../parameters"
  mock_outputs = {
    environment_name    = "temporary-dummy"
    private_domain_name = "temporary-dummy"
    key_alias_arn       = "temporary-dummy"
    s3_aws_vpc_endpoint = "temporary-dummy"
  }
}

dependencies {
  paths = ["../../parameters", "../../vault"]
}
