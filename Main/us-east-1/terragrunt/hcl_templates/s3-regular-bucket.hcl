// terraform {
//   source = "tfr://app.terraform.io/shieldfc/s3-vault-snapshots/aws//?version=0.1.0"
// }
terraform {
  source = "${local.base_source_url}?ref=v0.0.221"
}

locals {
  # Automatically load environment-level variables
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  env              = local.environment_vars.locals.environment
  customer_name    = local.environment_vars.locals.customer_name
  customer_id      = local.environment_vars.locals.customer_id
  base_source_url = "git::git@bitbucket.org:shielddev/terraform-modules.git//data-sources/s3-shield-archive"

  lifecycle_policies        = read_terragrunt_config("${dirname(find_in_parent_folders())}/hcl_templates/lifecycle_policies.hcl")
  lifecycle_delete_worm_14d = local.lifecycle_policies.locals.lifecycle_delete_worm_14d
  archive_bucket_policy     = local.lifecycle_policies.locals.archive_bucket_policy


}

inputs = {
  customer_name      = local.customer_name
  customer_id        = local.customer_id
  environment        = local.env
  lifecycle_rule     = local.lifecycle_delete_worm_14d
  bucket_name        = "s3-bucket-hr-data"
  custom_kms_key_arn = dependency.parameters.outputs.key_alias_arn
  s3_endpoint_id     = dependency.parameters.outputs.s3_aws_vpc_endpoint
  bucket_policy      = local.archive_bucket_policy # Policy to restrict all access except from VPC s3 endpoint
  # bucket_default_retention_mode = "COMPLIANCE"      #should be changed in producation  
  transcribe_archive   = false    

}

dependency "parameters" {
  config_path = "../parameters"
  mock_outputs = {
    private_domain_name = "temporary-dummy"
    key_alias_arn       = "temporary-dummy"
    s3_aws_vpc_endpoint = "temporary-dummy"
  }
}

dependencies {
  paths = ["../parameters"]
}
