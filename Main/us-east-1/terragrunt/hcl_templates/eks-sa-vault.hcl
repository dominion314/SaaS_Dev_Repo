terraform {
  source = "tfr://app.terraform.io/shieldfc/eks-sa-vault/k8s//?version=0.1.0"
}

locals {
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  env              = local.environment_vars.locals.environment
}

inputs = {
  cluster_name                     = local.env
  cluster_identity_oidc_issuer     = dependency.eks.outputs.aws_eks_cluster_oidc_issuer_url
  cluster_identity_oidc_issuer_arn = dependency.eks.outputs.aws_eks_oidc_provider_arn
  k8s_namespace                    = "vault"
  k8s_service_account_name         = "vault-snapshots"
  bucket_name                      = dependency.s3-vault-snapshots.outputs.bucket_name
  aws_eks_cluster_id               = dependency.eks.outputs.aws_eks_cluster_id
}

dependency "s3-vault-snapshots" {
  config_path = "../s3-vault-snapshots"
}

dependency "eks" {
  config_path = "../eks"
}

dependencies {
  paths = ["../eks", "../s3-vault-snapshots"]
}