locals {
  archive_bucket_policy = <<EOF
  {
    "Id": "VPCe",
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "VPCe",
        "Action": "s3:*",
        "Effect": "Deny",
        "Resource": [
          "$${bucket_arn}/*"
        ],
        "Condition": {
          "StringNotEquals": {
            "aws:SourceVpce": ["$${s3_endpoint_id}"]
          }
        },
        "Principal": "*"
      }
    ]
  }
  EOF

  lifecycle_rule_60d = [
    {
      id      = "delete-after-60-days"
      enabled = true
      expiration = [
        {
          days = 60
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 60
        }
      ]
    }
  ]

  lifecycle_rule_30d = [
    {
      id      = "delete-after-30-days"
      enabled = true
      expiration = [
        {
          days = 30
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 30
        }
      ]
    }
  ]

  lifecycle_rule_7d = [
    {
      id      = "delete-after-7-days"
      enabled = true
      expiration = [
        {
          days = 7
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 7
        }
      ]
    }
  ]

  lifecycle_rule_1825d = [
    {
      id      = "delete-after-1825-days"
      enabled = true
      expiration = [
        {
          days = 1825
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 1825
        }
      ]
    }
  ]

  lifecycle_delete_worm_60d = [
    {
      id                                     = "delete-onl-object"
      abort_incomplete_multipart_upload_days = 60
      enabled                                = true
      expiration = [
        {
          days                         = 60
          expired_object_delete_marker = true
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 60
        }
      ]
    },
    {
      id                                     = "delete-only"
      abort_incomplete_multipart_upload_days = 60
      enabled                                = true
      expiration = [
        {
          days                         = 60
          expired_object_delete_marker = true
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 60
        }
      ]
    }
  ]

  lifecycle_delete_worm_14d = [
    {
      id                                     = "delete-onl-object"
      abort_incomplete_multipart_upload_days = 14
      enabled                                = true
      expiration = [
        {
          days                         = 14
          expired_object_delete_marker = true
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 14
        }
      ]
    },
    {
      id                                     = "delete-only"
      abort_incomplete_multipart_upload_days = 14
      enabled                                = true
      expiration = [
        {
          days                         = 14
          expired_object_delete_marker = true
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 14
        }
      ]
    }
  ]
  lifecycle_delete_worm_1d = [
    {
      id                                     = "delete-onl-object"
      abort_incomplete_multipart_upload_days = 1
      enabled                                = true
      expiration = [
        {
          days                         = 1
          expired_object_delete_marker = true
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 1
        }
      ]
    },
    {
      id                                     = "delete-only"
      abort_incomplete_multipart_upload_days = 1
      enabled                                = true
      expiration = [
        {
          expired_object_delete_marker = true
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 1
        }
      ]
    }
  ]

  lifecycle_rule_1d = [
    {
      id      = "delete-after-1-day"
      enabled = true
      expiration = [
        {
          days = 1
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 1
        }
      ]
    }
  ]

  lifecycle_rule_365d = [
    {
      id      = "delete-after-365-days"
      enabled = true
      expiration = [
        {
          days = 365
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 365
        }
      ]
    }
  ]

    lifecycle_delete_worm_1095d = [
    {
      id                                     = "delete-onl-object"
      abort_incomplete_multipart_upload_days = 1095
      enabled                                = true
      expiration = [
        {
          days                         = 1095
          expired_object_delete_marker = true
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 1095
        }
      ]
    },
    {
      id                                     = "delete-only"
      abort_incomplete_multipart_upload_days = 1095
      enabled                                = true
      expiration = [
        {
          days                         = 1095
          expired_object_delete_marker = true
        }
      ]
      noncurrent_version_expiration = [
        {
          days = 1095
        }
      ]
    }
  ]
}