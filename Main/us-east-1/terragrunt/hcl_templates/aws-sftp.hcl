terraform {
  # source = "tfr://app.terraform.io/shieldfc/sftp-transfer-server/aws//?version=0.1.0"
  source = "../../modules/aws-sftp"
}

locals {
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  env              = local.environment_vars.locals.environment

  sftp_users = {
    "sftp_useraccess" = {
      user_name  = "sftp_useraccess",
      public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCku4Kdh0MYu4gP7iNz5UwqFgoRFbpjqVAee30fZeP3MdkkNIa6QBNDNLdkjNYwq4k3ria1uZp5NJ15/kk+B1SYPuAVoNjkLAYpP98DAm7y7AlrAw3NHnG9upyiOAYVUrrs9hklSIw40SBM41DjVeN6IlLzILhbstFUFUvmH16OOqH1rWVmmG8xbBQ3HOCCJTO2gfyVleuUyDY77SLfP+L58bdI9K42x11GTbn2gb8JtCeH8KkEJYdFG1CZlkmdOma6pL8He15gXm/Du03CvfrNc3ezlmCCysllX8e9JpH30l/wJV+7TpxsdsGuscMNI0UKYSO5biYIrz4d9qwyfZR3 exduser@exddvrh01nye.options-it.com"
    },
    "sftp_shield" = {
      user_name  = "sftp_shield",
      public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDAsbwmqSDBvizT/cmlYd3HSR/XSZsrZZhcdEdAIFE9sc8u9L4/6slYAfPTQxWpH4qO/TLvRXMkoAYlTCDqAdQBGsrdiqZnI/2Jozv+L8cDH+2WNJlQ0MEFL8dquwRycZslHR7gY9LV3427KbzkWIjxBDy5FT4X4jzeBrnSbiqcLXfCwdK2L+bPoKgZz6Zzn2YaRgKbfH14tkRGyy3K0RNpNbTsv4+P7AaCi8ZKQvZEdP5uAVRxLwqNNCZPHyMe5iZrU1lmmxLlMKPEXMHnDHhmXT4lCWEzCJhNgkvWlZ2IvH1sNT8fe5KHL/qywRyRQs+PS5v3BQkSJQBXG9hXZpKzyrqAvYB/dunnFZ2GPeN2q4Dl1kQJYE2qtGmh1RoPjyCkFppn9bUc4QF2C0ATMZsvF9Yo9Rr/NS7Ey8ZHgwR7gZ7rdQwgeHcEb9RD5BAGWihdtiG4i0SC6yGc5vH1/ar1t0uVzBsFmhO6X4mpi5D0xnmqacnHohzQqZAudExK0Ks="
    }
  }

  sftp_ingress = [{
    type        = "ingress"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Customer IP's
  }]
}

inputs = {

  subnet_ids           = dependency.parameters.outputs.public_subnets
  vpc_id               = dependency.parameters.outputs.vpc_id
  sftp_users           = local.sftp_users
  security_group_rules = local.sftp_ingress
  s3_bucket_name       = "32-prod-s3-sftp-datasources-3773e266ff7b"
  eip_enabled          = true

}

dependency "parameters" {
  config_path = "../parameters"
  mock_outputs = {
    public_subnets = "temporary-dummy-id"
    vpc_id         = "temporary-dummy-id"
  }
}

dependencies {
  paths = ["../parameters"]
}
