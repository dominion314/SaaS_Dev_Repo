terraform {
  source = "tfr://app.terraform.io/shieldfc/eks-argocd/k8s//?version=0.1.0"
}

inputs = {
  private_domain_name = dependency.parameters.outputs.private_domain_name
  aws_eks_cluster_id  = dependency.eks.outputs.aws_eks_cluster_id
}

dependency "parameters" {
  config_path = "../parameters"
  mock_outputs = {
    private_domain_name = "temporary-dummy-id"
  }
}

dependency "eks" {
  config_path = "../eks"
  mock_outputs = {
    aws_eks_cluster_id = "temporary-dummy-id"
  }
}


dependencies {
  paths = ["../parameters", "../eks"]
}

