resource "helm_release" "argocd_application" {
  count = var.enabled ? 1 : 0

  chart     = "${path.module}/helm/argocd-application"
  name      = var.helm_release_name
  namespace = var.argo_namespace

  values = [
    data.utils_deep_merge_yaml.values[0].output,
    data.utils_deep_merge_yaml.argo_application_values[0].output
  ]

  dynamic "set" {
    for_each = var.settings
    content {
      name  = set.key
      value = set.value
    }
  }
}


resource "kubernetes_ingress" "public_alb" {
  count = var.public_alb_ingress ? 1 : 0
  # wait_for_load_balancer = true
  metadata {
    name      = "alb-ingress-connect-nginx"
    namespace = "kube-system"
    annotations = {
      "alb.ingress.kubernetes.io/certificate-arn" : var.public_certificate_arn,
      "alb.ingress.kubernetes.io/healthcheck-path" : "/healthz",
      "alb.ingress.kubernetes.io/scheme" : "internet-facing",
      "alb.ingress.kubernetes.io/subnets" : join(",", var.public_subnets),
      "alb.ingress.kubernetes.io/security-groups" : var.internet_facing_sg,
      "kubernetes.io/ingress.class" : "alb",
      "alb.ingress.kubernetes.io/healthcheck-protocol" : "HTTP",
      "alb.ingress.kubernetes.io/wafv2-acl-arn" : var.wafv2_arn,
      "alb.ingress.kubernetes.io/ssl-policy" : "ELBSecurityPolicy-TLS-1-2-2017-01",

    }
  }
  spec {
    rule {
      http {
        path {
          path = "/*"
          backend {
            service_name = "ingress-nginx-controller"
            service_port = "http"
          }
        }
      }
    }
  }
}

# "alb.ingress.kubernetes.io/auth-idp-cognito" : jsonencode({ "userPoolARN" : var.cognito_user_pool_arn, "userPoolClientID" : var.cognito_user_pool_client_id, "userPoolDomain" : var.cognito_user_pool_domain })