data "aws_region" "current" {}

data "utils_deep_merge_yaml" "values" {
  count = var.enabled ? 1 : 0
  input = compact([
    local.values,
    var.values
  ])
}

data "utils_deep_merge_yaml" "argo_application_values" {
  count = var.enabled ? 1 : 0
  input = compact([
    yamlencode(local.argo_application_values),
    var.argo_application_values
  ])
}

data "kubernetes_ingress" "public_alb_output" {
  metadata {
    name = "alb-ingress-connect-nginx"
  }
}

output "kubernetes_ingress_outputs" {
  value = data.kubernetes_ingress.public_alb_output
}