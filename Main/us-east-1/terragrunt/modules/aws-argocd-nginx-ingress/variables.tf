variable "enabled" {
  type        = bool
  default     = true
  description = "Variable indicating whether deployment is enabled"
}

variable "helm_chart_name" {
  default = "ingress-nginx"
}

variable "helm_chart_version" {
  default = "4.0.17"
}

variable "helm_release_name" {
  default = "ingress-nginx"
}

variable "helm_repo_url" {
  default = "https://kubernetes.github.io/ingress-nginx"
}

variable "k8s_namespace" {
  type        = string
  default     = "kube-system"
  description = "The K8s namespace in which the nginx-ingress will be installed"
}

variable "settings" {
  type        = map(any)
  default     = {}
  description = "Additional settings which will be passed to the Helm chart values, see https://hub.helm.sh/charts/bitnami/nginx-ingress"
}

variable "values" {
  type        = string
  default     = ""
  description = "Additional yaml encoded values which will be passed to the Helm chart, see https://hub.helm.sh/charts/bitnami/nginx-ingress"
}

variable "argo_namespace" {
  type        = string
  default     = "argocd"
  description = "Namespace to deploy ArgoCD application CRD to"
}

variable "argo_application_values" {
  default     = ""
  description = "Value overrides to use when deploying argo application object with helm"
}

variable "argo_destionation_server" {
  type        = string
  default     = "https://kubernetes.default.svc"
  description = "Destination server for ArgoCD Application"
}

variable "argo_project" {
  type        = string
  default     = "default"
  description = "ArgoCD Application project"
}

variable "argo_info" {
  default = [{
    "name"  = "terraform"
    "value" = "true"
  }]
  description = "ArgoCD info manifest parameter"
}

variable "argo_sync_policy" {
  description = "ArgoCD syncPolicy manifest parameter"
  default     = {}
}

variable "private_certificate_arn" {
  type        = string
  default     = ""
  description = "Privagte certificate ARN for internal LB"
}

variable "public_certificate_arn" {
  type        = string
  default     = ""
  description = "Privagte certificate ARN for internal LB"
}

variable "private_subnets" {
  type        = list(string)
  default     = []
  description = "Private subnets list for internal LB"
}

variable "public_subnets" {
  type        = list(string)
  default     = []
  description = "Private subnets list for internal LB"
}

variable "public_alb_ingress" {
  type        = bool
  default     = false
  description = "Deploy public ALB ingress load balancer"
}

variable "internet_facing_sg" {
  type        = string
  default     = ""
  description = "Internet facing security group ID"
}

variable "wafv2_arn" {
  type        = string
  default     = ""
  description = "wafv2 arn to assosiate with alb"
}

variable "internal_alb_sg" {
  type        = string
  description = "Security group of the internal load balancer"
}

variable "cognito_user_pool_arn" {
  type        = string
  default     = ""
  description = "ARN for the Cognito User Pool"
}

variable "cognito_user_pool_client_id" {
  type        = string
  default     = ""
  description = "ID for the Cognito User Pool Client"
}

variable "cognito_user_pool_domain" {
  type        = string
  default     = ""
  description = "Name for the Cognito User Pool Domain"
}
