## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | >= 2.0 |
| helm | >= 1.0 |
| utils | >= 0.12.0 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 2.0 |
| helm | >= 1.0 |
| kubernetes | n/a |
| utils | >= 0.12.0 |

## Modules

No Modules.

## Resources

| Name |
|------|
| [aws_region](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) |
| [helm_release](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) |
| [kubernetes_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/ingress) |
| [utils_deep_merge_yaml](https://registry.terraform.io/providers/cloudposse/utils/latest/docs/data-sources/deep_merge_yaml) |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| argo\_application\_values | Value overrides to use when deploying argo application object with helm | `string` | `""` | no |
| argo\_destionation\_server | Destination server for ArgoCD Application | `string` | `"https://kubernetes.default.svc"` | no |
| argo\_info | ArgoCD info manifest parameter | `list` | <pre>[<br>  {<br>    "name": "terraform",<br>    "value": "true"<br>  }<br>]</pre> | no |
| argo\_namespace | Namespace to deploy ArgoCD application CRD to | `string` | `"argocd"` | no |
| argo\_project | ArgoCD Application project | `string` | `"default"` | no |
| argo\_sync\_policy | ArgoCD syncPolicy manifest parameter | `map` | `{}` | no |
| enabled | Variable indicating whether deployment is enabled | `bool` | `true` | no |
| helm\_chart\_name | n/a | `string` | `"ingress-nginx"` | no |
| helm\_chart\_version | n/a | `string` | `"4.0.9"` | no |
| helm\_release\_name | n/a | `string` | `"ingress-nginx"` | no |
| helm\_repo\_url | n/a | `string` | `"https://kubernetes.github.io/ingress-nginx"` | no |
| internet\_facing\_sg | Internet facing security group ID | `string` | `""` | no |
| k8s\_namespace | The K8s namespace in which the nginx-ingress will be installed | `string` | `"kube-system"` | no |
| private\_certificate\_arn | Privagte certificate ARN for internal LB | `string` | `""` | no |
| private\_subnets | Private subnets list for internal LB | `list(string)` | `[]` | no |
| public\_alb\_ingress | Deploy public ALB ingress load balancer | `bool` | `false` | no |
| settings | Additional settings which will be passed to the Helm chart values, see https://hub.helm.sh/charts/bitnami/nginx-ingress | `map(any)` | `{}` | no |
| values | Additional yaml encoded values which will be passed to the Helm chart, see https://hub.helm.sh/charts/bitnami/nginx-ingress | `string` | `""` | no |

## Outputs

No output.
