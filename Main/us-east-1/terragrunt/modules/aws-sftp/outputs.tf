output "transfer_endpoint" {
  description = "The endpoint of the Transfer Server"
  value       = join("", aws_transfer_server.default.*.endpoint)
}

output "elastic_ips" {
  description = "Provisioned Elastic IPs"
  value       = aws_eip.sftp.*.id
}
