locals {

  is_vpc                 = var.vpc_id != null
  security_group_enabled = var.security_group_enabled
  user_names             = keys(var.sftp_users)
  user_names_map         = { for idx, user in local.user_names : idx => user }
}

data "aws_s3_bucket" "landing" {

  bucket = var.s3_bucket_name
}

resource "aws_transfer_server" "default" {

  identity_provider_type = "SERVICE_MANAGED"
  protocols              = ["SFTP"]
  domain                 = var.domain
  endpoint_type          = local.is_vpc ? "VPC" : "PUBLIC"
  force_destroy          = var.force_destroy
  security_policy_name   = var.security_policy_name
  logging_role           = join("", aws_iam_role.logging[*].arn)

  dynamic "endpoint_details" {
    for_each = local.is_vpc ? [1] : []

    content {
      subnet_ids             = var.subnet_ids
      security_group_ids     = local.security_group_enabled ? module.security_group.*.id : var.vpc_security_group_ids
      vpc_id                 = var.vpc_id
      address_allocation_ids = var.eip_enabled ? aws_eip.sftp.*.id : var.address_allocation_ids
    }
  }

}

resource "aws_transfer_user" "default" {
  for_each = var.sftp_users

  server_id           = join("", aws_transfer_server.default[*].id)
  role                = aws_iam_role.s3_access_for_sftp_users[index(local.user_names, each.value.user_name)].arn
  user_name           = each.value.user_name
  home_directory_type = var.restricted_home ? "LOGICAL" : "PATH"
  dynamic "home_directory_mappings" {
    for_each = var.restricted_home ? [1] : []

    content {
      entry  = "/"
      target = "/${var.s3_bucket_name}/$${Transfer:UserName}"
    }
  }


}

resource "aws_transfer_ssh_key" "default" {
  for_each = var.sftp_users

  server_id = join("", aws_transfer_server.default[*].id)

  user_name = each.value.user_name
  body      = each.value.public_key

  depends_on = [
    aws_transfer_user.default
  ]
}

resource "aws_eip" "sftp" {
  count = var.eip_enabled ? length(var.subnet_ids) : 0

  vpc = local.is_vpc
}

module "security_group" {
  source = "./security_group"

  use_name_prefix = var.security_group_use_name_prefix
  rules           = var.security_group_rules
  description     = var.security_group_description
  vpc_id          = local.is_vpc ? var.vpc_id : null

  enabled = local.security_group_enabled

}

# Custom Domain
resource "aws_route53_record" "main" {
  count = length(var.domain_name) > 0 && length(var.zone_id) > 0 ? 1 : 0

  name    = var.domain_name
  zone_id = var.zone_id
  type    = "CNAME"
  ttl     = "300"

  records = [
    join("", aws_transfer_server.default[*].endpoint)
  ]
}

module "logging_label" {
  source = "./null-label"


  attributes = ["transfer", "cloudwatch"]

}

data "aws_iam_policy_document" "assume_role_policy" {

  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["transfer.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "s3_access_for_sftp_users" {
  for_each = local.user_names_map

  statement {
    sid    = "AllowListingOfUserFolder"
    effect = "Allow"

    actions = [
      "s3:ListBucket"
    ]

    resources = [
      join("", data.aws_s3_bucket.landing[*].arn)
    ]
  }

  statement {
    sid    = "HomeDirObjectAccess"
    effect = "Allow"

    actions = [
      "s3:PutObject",
      "s3:GetObject",
      "s3:DeleteObject",
      "s3:DeleteObjectVersion",
      "s3:GetObjectVersion",
      "s3:GetObjectACL",
      "s3:PutObjectACL"
    ]

    resources = [
      "${join("", data.aws_s3_bucket.landing[*].arn)}/${each.value}/*"
    ]
  }
}

data "aws_iam_policy_document" "logging" {

  statement {
    sid    = "CloudWatchAccessForAWSTransfer"
    effect = "Allow"

    actions = [
      "logs:CreateLogStream",
      "logs:DescribeLogStreams",
      "logs:CreateLogGroup",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }
}

module "iam_label" {
  for_each = local.user_names_map

  source = "./null-label"

  attributes = ["transfer", "s3", each.value]

}

resource "aws_iam_policy" "s3_access_for_sftp_users" {
  for_each = local.user_names_map

  name   = module.iam_label[index(local.user_names, each.value)].id
  policy = data.aws_iam_policy_document.s3_access_for_sftp_users[index(local.user_names, each.value)].json
}

resource "aws_iam_role" "s3_access_for_sftp_users" {
  for_each = local.user_names_map

  name = module.iam_label[index(local.user_names, each.value)].id

  assume_role_policy  = join("", data.aws_iam_policy_document.assume_role_policy[*].json)
  managed_policy_arns = [aws_iam_policy.s3_access_for_sftp_users[index(local.user_names, each.value)].arn]
}

resource "aws_iam_policy" "logging" {

  name   = module.logging_label.id
  policy = join("", data.aws_iam_policy_document.logging[*].json)
}

resource "aws_iam_role" "logging" {

  name                = module.logging_label.id
  assume_role_policy  = join("", data.aws_iam_policy_document.assume_role_policy[*].json)
  managed_policy_arns = [join("", aws_iam_policy.logging[*].arn)]
}

# Overall, this HCL file is configuring a Terraform module to create an SFTP server in AWS Transfer Family with various settings, users, security groups, DNS records, and IAM policies.

# The file defines several resources related to AWS Transfer Family, a service that provides a fully-managed SFTP (Secure File Transfer Protocol) solution. The resources being defined include:

# locals: defines some local variables to be used throughout the code, such as whether or not a VPC (Virtual Private Cloud) is being used and the names of SFTP users.

# data "aws_s3_bucket" "landing": defines a data source to retrieve information about an S3 bucket by its name.
# resource "aws_transfer_server" "default": defines an SFTP server in AWS Transfer Family, specifying various settings such as the identity provider type, protocols, domain, and security policy.

# resource "aws_transfer_user" "default": defines a user for the SFTP server, specifying settings such as the server ID, IAM role, home directory type, and home directory mappings.

# resource "aws_transfer_ssh_key" "default": defines an SSH key for the SFTP user.
# module "security_group": defines a security group for the SFTP server.
# resource "aws_route53_record" "main": defines a DNS record in Route 53 for the SFTP server.
# module "logging_label": defines a label to be used for logging.
# data "aws_iam_policy_document" "assume_role_policy": defines an IAM policy document for assuming a role.
# data "aws_iam_policy_document" "s3_access_for_sftp_users": defines an IAM policy document for accessing S3 buckets.
# data "aws_iam_policy_document" "logging": defines an IAM policy document for logging.
# module "iam_label": defines a label for IAM policies.
# resource "aws_iam_policy" "s3_access_for_sftp_users": defines an IAM policy for S3 access for SFTP users.
# resource "aws_iam_role" "s3_access_for_sftp_users": defines an IAM role for S3 access for SFTP users.
# resource "aws_iam_policy" "logging": defines an IAM policy for logging.
# resource "aws_iam_role" "logging": defines an IAM role for logging.

