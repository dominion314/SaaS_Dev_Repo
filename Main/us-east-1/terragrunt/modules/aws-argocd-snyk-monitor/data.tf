data "utils_deep_merge_yaml" "values" {
  count = var.enabled ? 1 : 0
  input = compact([
    local.values,
    var.values
  ])
}

data "utils_deep_merge_yaml" "argo_application_values" {
  count = var.enabled ? 1 : 0
  input = compact([
    yamlencode(local.argo_application_values),
    var.argo_application_values
  ])
}

data "aws_availability_zones" "available" {
}

data "aws_region" "current" {
}

data "aws_caller_identity" "current" {
}

# EKS
data "aws_eks_cluster" "this" {
  name = var.aws_eks_cluster_id
}

data "aws_eks_cluster_auth" "this" {
  name = var.aws_eks_cluster_id
}