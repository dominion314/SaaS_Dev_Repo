resource "helm_release" "argocd_application" {
  count = var.enabled ? 1 : 0

  chart     = "${path.module}/helm/argocd-application"
  name      = var.helm_release_name
  namespace = var.argo_namespace

  values = [
    data.utils_deep_merge_yaml.argo_application_values[0].output
  ]
}


resource "kubernetes_namespace" "snyk_monitor_namespace" {
  count = var.enabled ? 1 : 0
  metadata {
    labels = {
      mylabel = var.k8s_namespace
    }

    name = var.k8s_namespace
  }
}

resource "time_sleep" "wait_30_seconds" {
  depends_on = [helm_release.argocd_application]

  create_duration = "30s"
}


resource "kubernetes_secret_v1" "snyk_integration_token" {
  metadata {
    name      = "snyk-monitor"
    namespace = "snyk-monitor"
  }

  data = {
    integrationId    = var.snyk_k8s_token
    "dockercfg.json" = jsonencode({})
  }
}