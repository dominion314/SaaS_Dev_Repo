# snyk-monitor
variable "enabled" {
  type = bool
}

# Helm

variable "helm_chart_name" {
  default = "snyk-monitor"
}

variable "helm_chart_version" {
  default = "1.90.1"
}

variable "helm_release_name" {
  default = "snyk-monitor"
}

variable "helm_repo_url" {
  default = "https://charts.shieldfis.com/"
}

# K8S

variable "k8s_namespace" {
  default     = "snyk-monitor"
  description = "The k8s namespace in which the snyk_monitor service account has been created"
}

variable "k8s_service_account_name" {
  default     = "snyk-monitor"
  description = "The k8s snyk-monitor service account name"
}

variable "module_dependency" {
  default     = null
  description = "Dependence variable binds all AWS resources allocated by this module, dependent modules reference this variable"
}

variable "settings" {
  type        = map(any)
  default     = {}
  description = "Additional settings which will be passed to the Helm chart values"
}

variable "values" {
  type        = string
  default     = ""
  description = "Additional yaml encoded values which will be passed to the Helm chart"
}



# ArgoCD
variable "argo_namespace" {
  type        = string
  default     = "argocd"
  description = "Namespace to deploy ArgoCD application CRD to"
}

variable "argo_application_enabled" {
  type        = bool
  default     = true
  description = "If set to true, the module will be deployed as ArgoCD application, otherwise it will be deployed as a Helm release"
}

variable "argo_application_use_helm" {
  type        = bool
  default     = false
  description = "If set to true, the ArgoCD Application manifest will be deployed using Kubernetes provider as a Helm release. Otherwise it'll be deployed as a Kubernetes manifest. See Readme for more info"
}

variable "argo_application_values" {
  default     = ""
  description = "Value overrides to use when deploying argo application object with helm"
}

variable "argo_destionation_server" {
  type        = string
  default     = "https://kubernetes.default.svc"
  description = "Destination server for ArgoCD Application"
}

variable "argo_project" {
  type        = string
  default     = "default"
  description = "ArgoCD Application project"
}

variable "argo_info" {
  default = [{
    "name"  = "terraform"
    "value" = "true"
  }]
  description = "ArgoCD info manifest parameter"
}

variable "argo_sync_policy" {
  description = "ArgoCD syncPolicy manifest parameter"
  default     = {}
}

variable "snyk_k8s_token" {
  type        = string
  sensitive   = true
  description = "Snyk intergration token to k8s"
}


variable "aws_eks_cluster_id" {
  type = string
}
