## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | >= 2.0 |
| helm | >= 1.0 |
| utils | >= 0.12.0 |

## Providers

| Name | Version |
|------|---------|
| helm | >= 1.0 |
| kubernetes | n/a |
| time | n/a |
| utils | >= 0.12.0 |

## Modules

No Modules.

## Resources

| Name |
|------|
| [helm_release](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) |
| [kubernetes_manifest](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) |
| [kubernetes_namespace](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) |
| [time_sleep](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) |
| [utils_deep_merge_yaml](https://registry.terraform.io/providers/cloudposse/utils/latest/docs/data-sources/deep_merge_yaml) |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| argo\_application\_enabled | If set to true, the module will be deployed as ArgoCD application, otherwise it will be deployed as a Helm release | `bool` | `true` | no |
| argo\_application\_use\_helm | If set to true, the ArgoCD Application manifest will be deployed using Kubernetes provider as a Helm release. Otherwise it'll be deployed as a Kubernetes manifest. See Readme for more info | `bool` | `false` | no |
| argo\_application\_values | Value overrides to use when deploying argo application object with helm | `string` | `""` | no |
| argo\_destionation\_server | Destination server for ArgoCD Application | `string` | `"https://kubernetes.default.svc"` | no |
| argo\_info | ArgoCD info manifest parameter | `list` | <pre>[<br>  {<br>    "name": "terraform",<br>    "value": "true"<br>  }<br>]</pre> | no |
| argo\_namespace | Namespace to deploy ArgoCD application CRD to | `string` | `"argocd"` | no |
| argo\_project | ArgoCD Application project | `string` | `"default"` | no |
| argo\_sync\_policy | ArgoCD syncPolicy manifest parameter | `map` | `{}` | no |
| dashboard\_ingress\_enabled | Deploy dashboard ingress | `bool` | `false` | no |
| enabled | snyk-monitor | `bool` | n/a | yes |
| helm\_chart\_name | n/a | `string` | `"snyk_monitor"` | no |
| helm\_chart\_version | n/a | `string` | `"10.6.1"` | no |
| helm\_release\_name | n/a | `string` | `"snyk_monitor"` | no |
| helm\_repo\_url | n/a | `string` | `"https://helm.snyk_monitor.io/snyk_monitor"` | no |
| k8s\_namespace | The k8s namespace in which the snyk_monitor service account has been created | `string` | `"snyk_monitor"` | no |
| k8s\_service\_account\_name | The k8s snyk-monitor service account name | `string` | `"snyk_monitor"` | no |
| module\_dependency | Dependence variable binds all AWS resources allocated by this module, dependent modules reference this variable | `any` | `null` | no |
| private\_certificate\_arn | Privagte certificate ARN for internal LB | `string` | `""` | no |
| private\_subnet\_cidrs | Private Subnet CIDR's list for loadBalancerSourceRanges | `list(string)` | `[]` | no |
| settings | Additional settings which will be passed to the Helm chart values | `map(any)` | `{}` | no |
| snyk_monitor\_dashboard\_url | snyk_monitor dashboard url for ingress route | `string` | `""` | no |
| values | Additional yaml encoded values which will be passed to the Helm chart | `string` | `""` | no |

## Outputs

No output.
