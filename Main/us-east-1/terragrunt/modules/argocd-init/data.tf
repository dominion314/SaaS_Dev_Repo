data "aws_availability_zones" "available" {
}

data "aws_region" "current" {
}

data "aws_caller_identity" "current" {
}

# EKS

data "aws_eks_cluster" "this" {
  name = var.aws_eks_cluster_id
}

data "aws_eks_cluster_auth" "this" {
  name = var.aws_eks_cluster_id
}

data "kubernetes_secret" "ops_ssh_keys" {
  metadata {
    name      = "ops-ssh-keys"
    namespace = "default"
  }
}