resource "argocd_project" "infra_project" {
  metadata {
    name = "infra"
  }

  spec {
    description  = "infra applications project"
    source_repos = ["*"]

    destination {
      server    = "https://kubernetes.default.svc"
      namespace = "*"
    }

    cluster_resource_whitelist {
      group = "*"
      kind  = "*"
    }

    namespace_resource_whitelist {
      group = "*"
      kind  = "*"
    }

    role {
      name = "operations"
      policies = [
        "p, proj:infra:operations, applications, override, infra/*, allow",
        "p, proj:infra:operations, applications, sync, infra/*, allow",
      ]
    }
    role {
      name = "operations-elevated"
      policies = [
        "p, proj:infra:operations-elevated, applications, get, infra/*, allow",
        "p, proj:infra:operations-elevated, applications, sync, infra/*, deny",
      ]
    }
    sync_window {
      kind         = "allow"
      applications = ["*"]
      clusters     = ["*"]
      namespaces   = ["*"]
      duration     = "3600s"
      schedule     = "10 1 * * *"
      manual_sync  = true
    }
  }
}

resource "argocd_application" "infra" {
  metadata {
    name      = "infra"
    namespace = "argocd"
  }

  spec {
    project = argocd_project.infra_project.metadata.0.name

    source {
      repo_url        = "ssh://bitbucket.org/shielddev/gitops-${var.customer_name}-${var.environment_name}-aft"
      path            = "${data.aws_region.current.name}/argocd/infra"
      target_revision = "master"
      helm {
        parameter {
          name  = "repoUrl"
          value = "ssh://bitbucket.org/shielddev/gitops-${var.customer_name}-${var.environment_name}-aft"
        }
        parameter {
          name  = "targetRevision"
          value = "master"
        }
        parameter {
          name  = "privateCertificateArn"
          value = var.private_certificate_arn
        }
        parameter {
          name  = "publicCertificateArn"
          value = var.private_certificate_arn # TODO Replace to public 
        }
        parameter {
          name  = "localDomain"
          value = var.private_domain_name
        }
        parameter {
          name  = "publicDomain"
          value = var.public_domain
        }
        parameter {
          name  = "environment"
          value = var.environment_name
        }
        parameter {
          name  = "customerName"
          value = var.customer_name
        }
        values = <<EOT
          elasticsearch:
            data:
              replicas: 2
              persistence:
                size: 50Gi
          EOT
      }
    }

    destination {
      name      = "in-cluster"
      namespace = "infra"
    }

    sync_policy {
      automated = {
        prune     = true
        self_heal = true
      }
      sync_options = ["Validate=false"]
    }
  }
}
