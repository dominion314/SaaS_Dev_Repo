resource "null_resource" "environment" {
  provisioner "local-exec" {
    command = <<EOF
    git config --global --add safe.directory $(pwd)
    git commit -a -m "commit before environemnt changes"
    git push
    cd $(git rev-parse --show-toplevel)
    git checkout -b ${var.environment_name} template
    echo | openssl s_client \
      -servername NAME \
      -connect ${local.argocd_address} \
      | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' \
      > charts/shield/shield-config/certs/instance.crt
    cp charts/shield/shield-config/certs/instance.crt \
      charts/shield/models-config/certs/instance.crt
    keytool -import \
      -alias wc \
      -file charts/shield/shield-config/certs/instance.crt \
      -keystore charts/shield/shield-config/certs/truststore.jks \
      -storepass Shieldfc12! \
      -noprompt
    git add .
    git commit -a -m "environment ${var.environment_name} added"
    git push --set-upstream origin ${var.environment_name}
    git checkout master
    EOF
  }
}

resource "argocd_project" "environment_project" {
  metadata {
    name = var.environment_name
  }

  spec {
    description  = "${var.environment_name} applications project"
    source_repos = ["*"]

    destination {
      server    = "https://kubernetes.default.svc"
      namespace = "*"
    }
    # TODO Add project resources rules black/white lists (Mid)
    # cluster_resource_blacklist {
    #   group = "*"
    #   kind  = "*"
    # }
    cluster_resource_whitelist {
      group = "*"
      kind  = "*"
    }
    # namespace_resource_blacklist {
    #   group = "networking.k8s.io"
    #   kind  = "Ingress"
    # }
    namespace_resource_whitelist {
      group = "*"
      kind  = "*"
    }
    orphaned_resources {
      warn = true

      ignore {
        group = "rbac.authorization.k8s.io/v1"
        kind  = "Role"
        name  = "${var.environment_name}:operations"
      }
      ignore {
        group = "rbac.authorization.k8s.io/v1"
        kind  = "Role"
        name  = "${var.environment_name}:operations-elevated"
      }
      ignore {
        group = "rbac.authorization.k8s.io/v1"
        kind  = "RoleBinding"
        name  = "operations-${var.environment_name}-role-binding"
      }
      ignore {
        group = "rbac.authorization.k8s.io/v1"
        kind  = "RoleBinding"
        name  = "operations-elevated-${var.environment_name}-role-binding"
      }
    }
    # TODO Create custom permissions for operations and operations-elevated roles
    role {
      name = "operations"
      policies = [
        "p, proj:${var.environment_name}:operations, applications, override, ${var.environment_name}/*, allow",
        "p, proj:${var.environment_name}:operations, applications, sync, ${var.environment_name}/*, allow",
      ]
    }
    role {
      name = "operations-elevated"
      policies = [
        "p, proj:${var.environment_name}:operations-elevated, applications, get, ${var.environment_name}/*, allow",
        "p, proj:${var.environment_name}:operations-elevated, applications, sync, ${var.environment_name}/*, deny",
      ]
    }
    sync_window {
      kind         = "allow"
      applications = ["*"]
      clusters     = ["*"]
      namespaces   = ["*"]
      duration     = "3600s"
      schedule     = "10 1 * * *"
      manual_sync  = true
    }
    sync_window {
      kind         = "deny"
      applications = ["backend"]
      clusters = [
        "in-cluster"
      ]
      namespaces  = ["${var.environment_name}"]
      duration    = "12h"
      schedule    = "22 1 5 * *"
      manual_sync = false
    }
  }

  depends_on = [
    argocd_repository.private
  ]

}

resource "argocd_application" "environment" {
  metadata {
    name      = var.environment_name
    namespace = "argocd"
  }

  spec {
    project = argocd_project.environment_project.metadata.0.name

    source {
      repo_url        = "ssh://bitbucket.org/shielddev/gitops-${var.customer_name}-${var.environment_name}-aft"
      path            = "argocd/apps"
      target_revision = var.environment_name
      helm {
        parameter {
          name  = "repoUrl"
          value = "ssh://bitbucket.org/shielddev/gitops-${var.customer_name}-${var.environment_name}-aft"
        }
        parameter {
          name  = "project"
          value = argocd_project.environment_project.metadata.0.name
        }
        parameter {
          name  = "targetRevision"
          value = var.environment_name
        }
        parameter {
          name  = "namespace"
          value = var.environment_name
        }
        parameter {
          name  = "environmentId"
          value = var.vpc_internal_id
        }

        parameter {
          name  = "customerId"
          value = var.customer_id
        }
        parameter {
          name  = "publicUrl"
          value = format("app-%s%s.%s", var.customer_id, var.vpc_internal_id, var.public_domain)
        }
        parameter {
          name  = "imageRegistry"
          value = var.image_registry
        }
        parameter {
          name  = "privateCertificateArn"
          value = var.private_certificate_arn
        }
        parameter {
          name  = "publicCertificateArn"
          value = var.private_certificate_arn # TODO Replace to public 
        }
        parameter {
          name  = "localDomain"
          value = var.private_domain_name
        }
        parameter {
          name  = "fileSystemId"
          value = var.k8s_efs_vol
        }
        parameter {
          name  = "publicDomain"
          value = var.public_domain
        }
        parameter {
          name  = "environment"
          value = var.environment_name
        }
        parameter {
          name  = "customerName"
          value = var.customer_name
        }
        parameter {
          name  = "awsRegion"
          value = var.aws_region
        }
        parameter {
          name  = "vaultAddress"
          value = "https://vault.${var.private_domain_name}"
        }
        values = <<EOT
          elasticsearch:
            data:
              replicas: 2
              persistence:
                size: 50Gi
          EOT
      }
    }

    destination {
      name      = "in-cluster"
      namespace = var.environment_name
    }

    sync_policy {
      automated = {
        prune     = true
        self_heal = true
      }
      sync_options = ["Validate=false"]
    }
  }

  depends_on = [
    argocd_repository.private
  ]

}

# TODO Create Module
resource "kubernetes_storage_class" "efs_sc" {
  metadata {
    name = "efs-sc"
  }
  storage_provisioner = "efs.csi.aws.com"
  reclaim_policy      = "Retain"
  parameters = {
    provisioningMode = "efs-ap"
    fileSystemId     = var.k8s_efs_vol
    directoryPerms   = "700"
    gidRangeStart    = "1000"
    gidRangeEnd      = "2000"
  }
  mount_options = ["tls"]
}