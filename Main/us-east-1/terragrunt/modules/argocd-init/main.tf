
resource "argocd_repository" "private" {
  repo            = "ssh://bitbucket.org/shielddev/gitops-${var.customer_name}-${var.environment_name}-aft"
  ssh_private_key = data.kubernetes_secret.ops_ssh_keys.data["ops-secret-key"]
}