variable "sub_environment_list" {
  type        = list(string)
  default     = []
  description = "Sub Environments List"
}

#EKS
variable "aws_eks_cluster_id" {
  type = string
}
#parameters
variable "environment_name" {
  type = string
}
#parameters
variable "customer_name" {
  type = string
}
#parameters
variable "vpc_internal_id" {
  type = string
}
#parameters
variable "private_certificate_arn" {
  type = string
}
#parameters
variable "private_domain_name" {
  type = string
}
#parameters
variable "k8s_efs_vol" {
  type = string
}
#parameters
variable "public_domain" {
  type = string
}

variable "aws_region" {
  type        = string
  description = "AWS Region"
}

variable "customer_id" {
  type = string
}

variable image_registry {
  type        = string
  default     = "623704668857.dkr.ecr.eu-west-1.amazonaws.com"
  description = "Docker image registry (Default: 623704668857.dkr.ecr.eu-west-1.amazonaws.com - PROD REGISTRY)"
}
