resource "null_resource" "sub_environment" {
  for_each = toset(var.sub_environment_list)

  provisioner "local-exec" {
    command = <<EOF
    git commit -a -m "commit before sub environemnt changes"
    git push
    cd $(git rev-parse --show-toplevel)
    git checkout -b ${each.value} template
    echo | openssl s_client \
      -servername NAME \
      -connect ${local.argocd_address} \
      | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' \
      > charts/shield/shield-config/certs/instance.crt
    cp charts/shield/shield-config/certs/instance.crt \
      charts/shield/models-config/certs/instance.crt
    keytool -import \
      -alias wc \
      -file charts/shield/shield-config/certs/instance.crt \
      -keystore charts/shield/shield-config/certs/truststore.jks \
      -storepass Shieldfc12! \
      -noprompt
    git add .
    git commit -a -m "environment ${each.value} added"
    git push --set-upstream origin ${each.value}
    git checkout master
    EOF
  }
}

resource "argocd_project" "sub_environment_project" {
  for_each = toset(var.sub_environment_list)
  metadata {
    name = each.value
  }

  spec {
    description  = "${each.value} applications project"
    source_repos = ["*"]

    destination {
      server    = "https://kubernetes.default.svc"
      namespace = "*"
    }

    cluster_resource_whitelist {
      group = "*"
      kind  = "*"
    }

    namespace_resource_whitelist {
      group = "*"
      kind  = "*"
    }
    orphaned_resources {
      warn = true

      ignore {
        group = "rbac.authorization.k8s.io/v1"
        kind  = "Role"
        name  = "${each.value}:operations"
      }
      ignore {
        group = "rbac.authorization.k8s.io/v1"
        kind  = "Role"
        name  = "${each.value}:operations-elevated"
      }
      ignore {
        group = "rbac.authorization.k8s.io/v1"
        kind  = "RoleBinding"
        name  = "operations-${each.value}-role-binding"
      }
      ignore {
        group = "rbac.authorization.k8s.io/v1"
        kind  = "RoleBinding"
        name  = "operations-elevated-${each.value}-role-binding"
      }
    }
    role {
      name = "operations"
      policies = [
        "p, proj:${each.value}:operations, applications, override, ${each.value}/*, allow",
        "p, proj:${each.value}:operations, applications, sync, ${each.value}/*, allow",
      ]
    }
    role {
      name = "operations-elevated"
      policies = [
        "p, proj:${each.value}:operations-elevated, applications, get, ${each.value}/*, allow",
        "p, proj:${each.value}:operations-elevated, applications, sync, ${each.value}/*, deny",
      ]
    }
    sync_window {
      kind         = "allow"
      applications = ["*"]
      clusters     = ["*"]
      namespaces   = ["*"]
      duration     = "3600s"
      schedule     = "10 1 * * *"
      manual_sync  = true
    }
    sync_window {
      kind         = "deny"
      applications = ["backend"]
      clusters = [
        "in-cluster"
      ]
      namespaces  = ["${each.value}"]
      duration    = "12h"
      schedule    = "22 1 5 * *"
      manual_sync = false
    }
  }
}

resource "argocd_application" "sub_environment" {
  for_each = toset(var.sub_environment_list)
  metadata {
    name      = each.value
    namespace = "argocd"
  }

  spec {
    project = each.value

    source {
      repo_url        = "ssh://bitbucket.org/shielddev/gitops-${var.customer_name}-${var.environment_name}-aft"
      path            = "argocd/apps"
      target_revision = each.value
      helm {
        parameter {
          name  = "repoUrl"
          value = "ssh://bitbucket.org/shielddev/gitops-${var.customer_name}-${var.environment_name}-aft"
        }
        parameter {
          name  = "project"
          value = each.value
        }
        parameter {
          name  = "targetRevision"
          value = each.value
        }
        parameter {
          name  = "namespace"
          value = each.value
        }
        parameter {
          name  = "environmentId"
          value = var.vpc_internal_id
        }
        parameter {
          name  = "envUrl"
          value = format("%s-%s%s.%s", var.environment_name, var.customer_id, var.vpc_internal_id, var.public_domain)
        }
        parameter {
          name  = "customerId"
          value = var.customer_id
        }
        parameter {
          name  = "imageRegistry"
          value = var.image_registry
        }
        parameter {
          name  = "privateCertificateArn"
          value = var.private_certificate_arn
        }
        parameter {
          name  = "publicCertificateArn"
          value = var.private_certificate_arn # TODO Replace to public 
        }
        parameter {
          name  = "localDomain"
          value = var.private_domain_name
        }
        parameter {
          name  = "publicDomain"
          value = var.public_domain
        }
        parameter {
          name  = "environment"
          value = each.value
        }
        parameter {
          name  = "subEnv"
          value = true
        }
        parameter {
          name  = "customerName"
          value = var.customer_name
        }
        parameter {
          name  = "vaultAddress"
          value = "https://vault.${var.private_domain_name}"
        }
        values = <<EOT
          elasticsearch:
            data:
              replicas: 2
              persistence:
                size: 50Gi
          EOT
      }
    }

    destination {
      name      = "in-cluster"
      namespace = each.value
    }

    sync_policy {
      automated = {
        prune     = true
        self_heal = true
      }
      sync_options = ["Validate=true", "CreateNamespace=true"]
    }
  }
  depends_on = [
    argocd_project.sub_environment_project,
    null_resource.sub_environment
  ]
}