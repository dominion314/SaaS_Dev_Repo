terraform {
  required_providers {
    argocd = {
      source  = "oboukili/argocd"
      version = "2.1.0"
    }
  }
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.this.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.this.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.this.token
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.this.endpoint
    token                  = data.aws_eks_cluster_auth.this.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.this.certificate_authority.0.data)
  }
}

provider "argocd" {
  server_addr = local.argocd_address
  username    = local.argocd_username
  insecure    = false # env ARGOCD_INSECURE
  # auth_token  = "1234..."     # env ARGOCD_AUTH_TOKEN
  # password  = ""              # env ARGOCD_AUTH_PASSWORD
}