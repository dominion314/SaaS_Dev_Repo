data "aws_route53_zone" "main" {
  provider = aws.network
  name     = var.zone_name
}

resource "aws_acm_certificate" "main" {
  provider          = aws.us
  domain_name       = var.domain_name
  validation_method = "DNS"

  tags = {
    Name        = var.domain_name
    Environment = var.environment
    Automation  = "Terraform"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "main" {
  provider = aws.network
  for_each = {
    for dvo in aws_acm_certificate.main.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }
  zone_id = data.aws_route53_zone.main.id
  ttl     = "60"
  name    = each.value.name
  type    = each.value.type
  records = [each.value.record]
}