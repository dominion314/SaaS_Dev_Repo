import boto3
import json
from botocore.exceptions import ClientError
import time

def get_parameter_value(ssm_client, parameter_name):
    """Get parameter value with error handling"""
    try:
        response = ssm_client.get_parameter(
            Name=parameter_name,
            WithDecryption=True  # This will decrypt SecureString parameters
        )
        return {
            'name': parameter_name,
            'value': response['Parameter']['Value'],
            'type': response['Parameter']['Type']
        }
    except ClientError as e:
        print(f"Error getting parameter {parameter_name}: {e}")
        return None

def main():
    # Configure AWS client
    session = boto3.Session(profile_name='param-migrate', region_name='us-east-2')
    ssm = session.client('ssm')
    
    # Read parameter names from file
    with open('parameters_list.txt', 'r') as f:
        parameters = [line.strip() for line in f if line.strip()]
    
    # Get values for each parameter
    parameter_data = []
    for param_name in parameters:
        print(f"Getting value for: {param_name}")
        param_info = get_parameter_value(ssm, param_name)
        if param_info:
            parameter_data.append(param_info)
        time.sleep(0.1)  # Small delay to avoid API throttling
    
    # Save to JSON file
    with open('parameter_values.json', 'w') as f:
        json.dump(parameter_data, f, indent=2)
    
    print(f"\nExported {len(parameter_data)} parameters to parameter_values.json")

if __name__ == "__main__":
    main()