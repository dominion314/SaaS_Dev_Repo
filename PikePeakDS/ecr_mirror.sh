#!/bin/bash

# AWS account credentials
export AWS_COMMERCIAL_PROFILE="dom-amplify"
export AWS_GOVCLOUD_PROFILE="govcloud"

# Function to check if repository exists in GovCloud
check_repo_exists() {
    local repository_name=$1
    
    if aws ecr describe-repositories \
        --repository-names "$repository_name" \
        --region us-gov-west-1 \
        --profile $AWS_GOVCLOUD_PROFILE >/dev/null 2>&1; then
        return 0  # Repository exists
    else
        return 1  # Repository doesn't exist
    fi
}

# Function to create repository in GovCloud
create_repo() {
    local repository_name=$1
    
    echo "Creating repository $repository_name in GovCloud..."
    aws ecr create-repository \
        --repository-name "$repository_name" \
        --region us-gov-west-1 \
        --profile $AWS_GOVCLOUD_PROFILE \
        --image-tag-mutability MUTABLE \
        --image-scanning-configuration scanOnPush=true || {
            echo "Failed to create repository $repository_name"
            return 1
        }
}

# Function to check if image exists in GovCloud ECR
check_image_exists() {
    local repository_name=$1
    
    # Try to describe the images, return true if image exists
    if aws ecr describe-images \
        --repository-name "$repository_name" \
        --image-ids imageTag=latest \
        --region us-gov-west-1 \
        --profile $AWS_GOVCLOUD_PROFILE >/dev/null 2>&1; then
        return 0  # Image exists
    else
        return 1  # Image doesn't exist
    fi
}

# Define source and destination URIs for each image using arrays
SOURCE_URIS=(
    "482718065396.dkr.ecr.us-east-2.amazonaws.com/samgatherer"
    "482718065396.dkr.ecr.us-east-2.amazonaws.com/pastmapper"
    "482718065396.dkr.ecr.us-east-2.amazonaws.com/samfinder"
    "482718065396.dkr.ecr.us-east-2.amazonaws.com/sammapper"
    "482718065396.dkr.ecr.us-east-2.amazonaws.com/samdetailed"
    "482718065396.dkr.ecr.us-east-2.amazonaws.com/returninfo"
    "482718065396.dkr.ecr.us-east-2.amazonaws.com/pulldocument"
    "482718065396.dkr.ecr.us-east-2.amazonaws.com/llm"
    "482718065396.dkr.ecr.us-east-2.amazonaws.com/indepth"
    "482718065396.dkr.ecr.us-east-2.amazonaws.com/docrouting"
    "482718065396.dkr.ecr.us-east-2.amazonaws.com/docmanagement"
)

DEST_URIS=(
    "271945552223.dkr.ecr.us-gov-west-1.amazonaws.com/samgatherer"
    "271945552223.dkr.ecr.us-gov-west-1.amazonaws.com/pastmapper"
    "271945552223.dkr.ecr.us-gov-west-1.amazonaws.com/samfinder"
    "271945552223.dkr.ecr.us-gov-west-1.amazonaws.com/sammapper"
    "271945552223.dkr.ecr.us-gov-west-1.amazonaws.com/samdetailed"
    "271945552223.dkr.ecr.us-gov-west-1.amazonaws.com/returninfo"
    "271945552223.dkr.ecr.us-gov-west-1.amazonaws.com/pulldocument"
    "271945552223.dkr.ecr.us-gov-west-1.amazonaws.com/llm"
    "271945552223.dkr.ecr.us-gov-west-1.amazonaws.com/indepth"
    "271945552223.dkr.ecr.us-gov-west-1.amazonaws.com/docrouting"
    "271945552223.dkr.ecr.us-gov-west-1.amazonaws.com/docmanagement"
)

# Test network connectivity
echo "Testing network connectivity..."
if ! podman run --rm alpine nslookup 482718065396.dkr.ecr.us-east-2.amazonaws.com; then
    echo "DNS resolution failed. Please check network configuration."
    exit 1
fi

echo "Step 1: Logging into Commercial ECR..."
aws ecr get-login-password --region us-east-2 --profile $AWS_COMMERCIAL_PROFILE | \
    podman login --username AWS --password-stdin 482718065396.dkr.ecr.us-east-2.amazonaws.com

echo "Step 2: Logging into GovCloud ECR..."
aws ecr get-login-password --region us-gov-west-1 --profile $AWS_GOVCLOUD_PROFILE | \
    podman login --username AWS --password-stdin 271945552223.dkr.ecr.us-gov-west-1.amazonaws.com

# Mirror each image
for i in "${!SOURCE_URIS[@]}"; do
    SOURCE_URI="${SOURCE_URIS[$i]}"
    DEST_URI="${DEST_URIS[$i]}"
    
    # Extract repository name from URI
    REPO_NAME=$(echo "$DEST_URI" | cut -d'/' -f2)
    
    # Check if repository exists in GovCloud
    if ! check_repo_exists "$REPO_NAME"; then
        echo "Repository $REPO_NAME doesn't exist in GovCloud"
        if ! create_repo "$REPO_NAME"; then
            echo "Failed to create repository $REPO_NAME, skipping..."
            continue
        fi
        echo "Successfully created repository $REPO_NAME"
    fi
    
    echo "Checking $REPO_NAME in GovCloud..."
    
    if check_image_exists "$REPO_NAME"; then
        echo "Image $REPO_NAME:latest already exists in GovCloud, skipping..."
        continue
    fi
    
    echo "Processing image: $SOURCE_URI"
    
    # Pull from commercial
    echo "Pulling from Commercial ECR..."
    podman pull "${SOURCE_URI}:latest" || {
        echo "Failed to pull ${SOURCE_URI}"
        continue
    }
    
    # Tag for GovCloud
    echo "Tagging for GovCloud..."
    podman tag "${SOURCE_URI}:latest" "${DEST_URI}:latest" || {
        echo "Failed to tag ${SOURCE_URI}"
        continue
    }
    
    # Push to GovCloud
    echo "Pushing to GovCloud..."
    podman push "${DEST_URI}:latest" || {
        echo "Failed to push to ${DEST_URI}"
        continue
    }
    
    echo "Successfully mirrored ${SOURCE_URI} to ${DEST_URI}"
    echo "----------------------------------------"
done