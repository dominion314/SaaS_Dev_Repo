variable "env" {
  description = "The environment for the deployment (e.g., dev, prod)"
  type        = string
  default     = "govcloud-dev"
}

variable "aws_region" {
  description = "The AWS region to deploy the database"
  type        = string
  default     = "us-gov-west-1"
}

variable "db_instance_class" {
  description = "The instance class for the database"
  type        = string
  default     = "db.t3.micro"  # You can adjust this as needed
}

variable "db_name" {
  description = "The name of the database"
  type        = string
  default     = "pikes_peak"  # You can leave this empty if you want to use the default name
}

variable "db_username" {
  description = "The username for the database"
  type        = string
  default     = "pikespeak"  # You can leave this empty if you want to use the default username
}

variable "db_password" {
  description = "The password for the database"
  type        = string
  sensitive   = true 
  default     = "Pikespeak"  # You can leave this empty if you want to use the default password
}

variable "db_allocated_storage" {
  description = "The allocated storage for the database in GB"
  type        = number
  default     = 10  # Adjust based on need
}