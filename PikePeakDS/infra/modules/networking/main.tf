resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "${var.env}-vpc"
  }
}

resource "aws_subnet" "private" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "${var.aws_region}a"

  tags = {
    Name = "${var.env}-private-subnet"
  }
}


resource "aws_security_group" "main" {
  name        = "${var.env}-main-sg"
  description = "Main security group"
  vpc_id      = aws_vpc.main.id

  # This will allow all outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # This is to allow internal VPC traffic
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [aws_vpc.main.cidr_block]
  }

  tags = {
    Name = "${var.env}-main-sg"
  }
}

#########
#########

# VPC Endpoints for SageMaker and CloudWatch
resource "aws_vpc_endpoint" "cloudwatch_logs" {
  vpc_id            = aws_vpc.main.id  # Reference to your existing VPC
  service_name      = "com.amazonaws.us-gov-west-1.logs"
  vpc_endpoint_type = "Interface"
  
  security_group_ids = [aws_security_group.main.id]  # Reference to your existing security group
  subnet_ids         = [aws_subnet.private.id]  # Reference to your existing private subnet
  
  private_dns_enabled = true
}

resource "aws_vpc_endpoint" "sagemaker_api" {
  vpc_id            = aws_vpc.main.id
  service_name      = "com.amazonaws.us-gov-west-1.sagemaker.api"
  vpc_endpoint_type = "Interface"
  
  security_group_ids = [aws_security_group.main.id]
  subnet_ids         = [aws_subnet.private.id]
  
  private_dns_enabled = true
}

resource "aws_vpc_endpoint" "sagemaker_runtime" {
  vpc_id            = aws_vpc.main.id
  service_name      = "com.amazonaws.us-gov-west-1.sagemaker.runtime"
  vpc_endpoint_type = "Interface"
  
  security_group_ids = [aws_security_group.main.id]
  subnet_ids         = [aws_subnet.private.id]
  
  private_dns_enabled = true
}