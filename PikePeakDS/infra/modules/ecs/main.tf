resource "aws_ecs_cluster" "bdtool_cluster" {
  name = "BDTool_gov_dev"
  #capacity_providers = ["FARGATE", "FARGATE_SPOT"] # I am unable to deploy FARGATE_SPOT, need to figure out why.
  tags = {
    Name = "BDTool_gov_dev"
  }
}