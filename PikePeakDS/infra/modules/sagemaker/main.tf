# # Create ECR Repository
# resource "aws_ecr_repository" "ironbank_pytorch" {
#   name                 = "ironbank-pytorch"
#   image_tag_mutability = "MUTABLE"

#   image_scanning_configuration {
#     scan_on_push = true
#   }
# }

resource "aws_s3_bucket" "model_artifacts" {
  bucket = "sagemaker-model-artifacts-${var.env}"
}

resource "aws_s3_object" "model" {
  bucket = aws_s3_bucket.model_artifacts.id
  key    = "model.tar.gz"
  source = "${path.module}/model/model.tar.gz"  # This is correct if tarball is in model/
  etag   = filemd5("${path.module}/model/model.tar.gz")
}

data "aws_ecr_repository" "ironbank_sagemaker_base" {
  name = "ironbank-sagemaker-base"
}

# Reference existing ECR Repository
data "aws_ecr_repository" "ironbank_pytorch" {
  name = "ironbank-pytorch"
}

data "aws_subnet" "private" {
  id = var.subnet_id  # You'll need to add this variable
}

data "aws_security_group" "main" {
  id = var.security_group_id  # You'll need to add this variable
}

# Add ECR policy to allow pulls
resource "aws_ecr_repository_policy" "ironbank_pytorch_policy" {
  repository = data.aws_ecr_repository.ironbank_pytorch.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid    = "AllowPull"
        Effect = "Allow"
        Principal = {
          AWS = aws_iam_role.sagemaker_execution_role.arn
        }
        Action = [
          "ecr:BatchGetImage",
          "ecr:GetDownloadUrlForLayer"
        ]
      }
    ]
  })
}

# Model Registry
resource "aws_sagemaker_model" "llm_model" {
  name               = "llm-model-${var.env}"
  execution_role_arn = aws_iam_role.sagemaker_execution_role.arn

  primary_container {
    image          = "${data.aws_ecr_repository.ironbank_pytorch.repository_url}:latest"
    mode           = "SingleModel"
    model_data_url = "s3://${aws_s3_bucket.model_artifacts.bucket}/${aws_s3_object.model.key}"
    environment = {
      SAGEMAKER_PROGRAM           = "serve"  # Critical change - entry point script
      SAGEMAKER_SUBMIT_DIRECTORY  = "/home/model-server"  # Standard PyTorch Serving location
      TS_SERVICE_HANDLER          = "inference.py::handle"  # Explicit handler specification
      SAGEMAKER_CONTAINER_LOG_LEVEL = "DEBUG"
    }
  }
}

# SageMaker Execution Role
resource "aws_iam_role" "sagemaker_execution_role" {
  name = "sagemaker-execution-role-${var.env}"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "sagemaker.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_ecr_repository_policy" "ironbank_base_policy" {
  repository = data.aws_ecr_repository.ironbank_sagemaker_base.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid    = "AllowPull"
        Effect = "Allow"
        Principal = {
          AWS = aws_iam_role.sagemaker_execution_role.arn
        }
        Action = [
          "ecr:BatchGetImage",
          "ecr:GetDownloadUrlForLayer",
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "ecr:BatchCheckLayerAvailability"
        ]
      }
    ]
  })
}

# Add S3 access policy
resource "aws_iam_role_policy" "sagemaker_s3_access" {
  name = "sagemaker-s3-access-${var.env}"
  role = aws_iam_role.sagemaker_execution_role.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "s3:GetObject",
          "s3:ListBucket",
          "s3:PutObject"
        ]
        Resource = [
          aws_s3_bucket.model_artifacts.arn,
          "${aws_s3_bucket.model_artifacts.arn}/*"
        ]
      }
    ]
  })
}

# Add EC2 permissions policy
resource "aws_iam_role_policy" "sagemaker_vpc_access" {
  name = "sagemaker-vpc-access-${var.env}"
  role = aws_iam_role.sagemaker_execution_role.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "ec2:CreateNetworkInterface",
          "ec2:CreateNetworkInterfacePermission",
          "ec2:DeleteNetworkInterface",
          "ec2:DeleteNetworkInterfacePermission",
          "ec2:DescribeNetworkInterfaces",
          "ec2:DescribeVpcs",
          "ec2:DescribeDhcpOptions",
          "ec2:DescribeSubnets",
          "ec2:DescribeSecurityGroups"
        ]
        Resource = "*"
      }
    ]
  })
}

# Required IAM policies for SageMaker
resource "aws_iam_role_policy" "sagemaker_policy" {
  name = "sagemaker-policy-${var.env}"
  role = aws_iam_role.sagemaker_execution_role.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "s3:GetObject",
          "s3:PutObject",
          "s3:ListBucket"
        ]
        Resource = [
          "arn:aws-us-gov:s3:::${var.model_artifact_bucket}",
          "arn:aws-us-gov:s3:::${var.model_artifact_bucket}/*"
        ]
      },
      {
        Effect = "Allow"
        Action = [
          "ecr:GetAuthorizationToken",
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "ecr:BatchCheckLayerAvailability"
        ]
        Resource = ["*"]
      }
    ]
  })
}

# Endpoint Configuration
resource "aws_sagemaker_endpoint_configuration" "llm_endpoint_config" {
  name = "llm-endpoint-config-${var.env}"

  production_variants {
    variant_name           = "AllTraffic"
    model_name            = aws_sagemaker_model.llm_model.name
    initial_instance_count = 1
    instance_type         = "ml.t2.medium"
  }

  # Add explicit dependency on model
  depends_on = [aws_sagemaker_model.llm_model]
}

# Endpoint
resource "aws_sagemaker_endpoint" "llm_endpoint" {
  name                 = "llm-endpoint-${var.env}"
  endpoint_config_name = aws_sagemaker_endpoint_configuration.llm_endpoint_config.name

  # Add explicit dependency on both model and config
  depends_on = [
    aws_sagemaker_model.llm_model,
    aws_sagemaker_endpoint_configuration.llm_endpoint_config
  ]
}

# Add CloudWatch logging permissions
resource "aws_iam_role_policy_attachment" "sagemaker_cloudwatch" {
  role       = aws_iam_role.sagemaker_execution_role.name
  policy_arn = "arn:aws-us-gov:iam::aws:policy/CloudWatchLogsFullAccess"
}