variable "env" {
  description = "Environment name"
  type        = string
}

variable "iron_bank_registry" {
  description = "Iron Bank container registry URL"
  type        = string
}

variable "iron_bank_registry_arn" {
  description = "Iron Bank container registry ARN"
  type        = string
}

variable "pytorch_version" {
  description = "PyTorch version to use from Iron Bank"
  type        = string
  default     = "2.0.1"
}

variable "model_artifact_bucket" {
  description = "S3 bucket containing model artifacts"
  type        = string
}

variable "instance_type" {
  description = "SageMaker instance type"
  type        = string
  default     = "ml.g4dn.xlarge"
}

variable "instance_count" {
  description = "Number of instances"
  type        = number
  default     = 1
}

variable "subnet_id" {
  description = "ID of the private subnet for SageMaker"
  type        = string
}

variable "security_group_id" {
  description = "ID of the security group for SageMaker"
  type        = string
}