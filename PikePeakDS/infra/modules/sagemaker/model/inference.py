# model/inference.py
from http.server import HTTPServer, BaseHTTPRequestHandler
import json

class ModelHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/ping':
            # Health check endpoint
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            response = {"status": "ok"}
            self.wfile.write(json.dumps(response).encode())
        else:
            self.send_response(404)
            self.end_headers()

    def do_POST(self):
        if self.path == '/invocations':
            # Basic response for now
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            response = {"message": "Model is running"}
            self.wfile.write(json.dumps(response).encode())
        else:
            self.send_response(404)
            self.end_headers()

def run_server():
    print("Starting server on port 8080...")
    server = HTTPServer(('', 8080), ModelHandler)
    server.serve_forever()

if __name__ == '__main__':
    run_server()