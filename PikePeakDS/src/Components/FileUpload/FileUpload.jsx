import React, { useState } from 'react';
// import shortid from "https://cdn.skypack.dev/shortid@2.2.16";
import './FileUpload.scss';
import uploadImg from '../../assets/images/upload-file.svg';
import { v4 as uuidv4 } from 'uuid';



const FileUpload = ({ onUpload, className }) => {
    const [selectedFiles, setSelectedFiles] = useState([]);

    const filesizes = (bytes, decimals = 2) => {
        if (bytes === 0) return '0 Bytes';
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    };

    const inputChange = (e) => {
        let files = Array.from(e.target.files);
        files.forEach(file => {
            let reader = new FileReader();
            reader.onloadend = () => {
                const newUuid = uuidv4();
                console.log(newUuid);
                setSelectedFiles(prev => [
                    ...prev,
                    {
                        id: newUuid,
                        filename: file.name,
                        filetype: file.type,
                        filecontent: reader.result.split(',')[1],
                        datetime: file.lastModifiedDate.toLocaleString('en-IN'),
                        filesize: filesizes(file.size),
                        s3key: `${newUuid}_${file.name}`,
                        category: '' 
                    }
                ]);
            };
            reader.readAsDataURL(file);
        });
    };

    const handleCategoryChange = (id, category) => {
        setSelectedFiles(prevFiles => prevFiles.map(file => 
            file.id === id ? { ...file, category: category } : file
        ));
    };

    const deleteSelectedFile = (id) => {
        if (window.confirm("Are you sure you want to delete this file?")) {
            const newFiles = selectedFiles.filter(file => file.id !== id);
            setSelectedFiles(newFiles);
        }
    };

    const fileUploadSubmit = async (e) => {
        e.preventDefault();
        if (selectedFiles.length > 0) {
            const payload = {
                files: selectedFiles.map(file => ({
                    id: file.id,
                    filename: file.filename,
                    filetype: file.filetype,
                    content: file.filecontent,
                    datetime: file.datetime,
                    filesize: file.filesize,
                    category: file.category,
                    s3key:file.s3key
                }))
            };
            onUpload(true, payload);
            setSelectedFiles([]);
        } else {
            onUpload(false, 'Please select a file to upload.');
            alert('Please select a file to upload');
        }
    };

    return (
        <div className={`fileupload-view ${className}`}>
            <div className="fileupload-card">
                <form onSubmit={fileUploadSubmit}>
                    <div className="kb-file-upload">
                        <h6>Upload multiple files</h6>
                        <div className="file-upload-box">
                            <input type="file" id="fileupload" className="file-upload-input" onChange={inputChange} multiple />
                            <div className="upload-box">
                                <img src={uploadImg} alt="Upload" />
                                <p>Drop files here or <span className="file-link">browse</span> from your computer.</p>
                            </div>
                        </div>
                    </div>
                    <div className="kb-attach-box">
                        {selectedFiles.map(file => (
                            <div className="file-atc-box" key={file.id}>
                                <div className="file-detail">
                                    <h6>{file.filename}</h6>
                                    <p><span>Size: {file.filesize}</span><span className="ml-2">Modified Time: {file.datetime}</span></p>
                                    <select value={file.category} onChange={(e) => handleCategoryChange(file.id, e.target.value)}>
                                        <option value="">Select Category</option>
                                        <option value="Past Performance">Past Performance</option>
                                        <option value="Past PWS/SOW">Past PWS/SOW</option>
                                    </select>
                                    <div className="file-actions">
                                        <button type="button" className="file-action-btn" onClick={() => deleteSelectedFile(file.id)}>Delete</button>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                    <div className="kb-buttons-box">
                        <button type="submit" className="form-submit">Upload Documents</button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default FileUpload;

// import React, { useState } from 'react';
// import shortid from "https://cdn.skypack.dev/shortid@2.2.16";
// import './FileUpload.scss';
// import uploadImg from '../../assets/images/upload-file.svg';

// const FileUpload = (props) => {

//     const [selectedfile, SetSelectedFile] = useState([]);
//     const [Files, SetFiles] = useState([]);


//     const filesizes = (bytes, decimals = 2) => {
//         if (bytes === 0) return '0 Bytes';
//         const k = 1024;
//         const dm = decimals < 0 ? 0 : decimals;
//         const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
//         const i = Math.floor(Math.log(bytes) / Math.log(k));
//         return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
//     }

//     const InputChange = (e) => {
//         // --For Multiple File Input
//         let images = [];
//         for (let i = 0; i < e.target.files.length; i++) {
//             images.push((e.target.files[i]));
//             let reader = new FileReader();
//             let file = e.target.files[i];
//             reader.onloadend = () => {
//                 SetSelectedFile((preValue) => {
//                     return [
//                         ...preValue,
//                         {
//                             id: shortid.generate(),
//                             filename: e.target.files[i].name,
//                             filetype: e.target.files[i].type,
//                             fileimage: reader.result,
//                             datetime: e.target.files[i].lastModifiedDate.toLocaleString('en-IN'),
//                             filesize: filesizes(e.target.files[i].size)
//                         }
//                     ]
//                 });
//             }
//             if (e.target.files[i]) {
//                 reader.readAsDataURL(file);
//             }
//         }
//     }


//     const DeleteSelectFile = (id) => {
//         if(window.confirm("Are you sure you want to delete this Image?")){
//             const result = selectedfile.filter((data) => data.id !== id);
//             SetSelectedFile(result);
//         }else{
//             // alert('No');
//         }
        
//     }

//     const FileUploadSubmit = async (e) => {
//         e.preventDefault();

//         // form reset on submit 
//         e.target.reset();
//         if (selectedfile.length > 0) {
//             for (let index = 0; index < selectedfile.length; index++) {
//                 SetFiles((preValue)=>{
//                     return[
//                         ...preValue,
//                         selectedfile[index]
//                     ]   
//                 })
//             }
//             SetSelectedFile([]);
//         } else {
//             alert('Please select file')
//         }
//     }


//     const DeleteFile = async (id) => {
//         if(window.confirm("Are you sure you want to delete this Image?")){
//             const result = Files.filter((data)=>data.id !== id);
//             SetFiles(result);
//         }else{
//             // alert('No');
//         }
//     }

//     return (
//         <div className={`fileupload-view ${props.className}`}>
//             <div className="fileupload-card">
//                 <form onSubmit={FileUploadSubmit}>
//                     <div className="kb-file-upload">
//                         <div className="kb-data-title">
//                             <h6>Upload multiple files</h6>
//                         </div>
//                         <div className="file-upload-box">
//                             <input type="file" id="fileupload" className="file-upload-input" onChange={InputChange} multiple />
//                             <div className="upload-box">
//                                 <div className="upload-img">
//                                     <img src={uploadImg} alt="uploadImg" />
//                                 </div>
//                                 <h3 className="heading">Drop files here</h3>
//                                 <span>Or <span className="file-link">Browse Files</span> from your computer.</span>
//                             </div>
//                         </div>
//                     </div>
//                     <div className="kb-attach-box">
//                         {
//                             selectedfile.map((data, index) => {
//                                 const { id, filename, filetype, fileimage, datetime, filesize } = data;
//                                 return (
//                                     <div className="file-atc-box" key={id}>
//                                         {
//                                             filename.match(/.(jpg|jpeg|png|gif|svg)$/i) ?
//                                                 <div className="file-image"> <img src={fileimage} alt="" /></div> :
//                                                 <div className="file-image"><i className="far fa-file-alt"></i></div>
//                                         }
//                                         <div className="file-detail">
//                                             <h6>{filename}</h6>
//                                             <p></p>
//                                             <p><span>Size : {filesize}</span><span className="ml-2">Modified Time : {datetime}</span></p>
//                                             <div className="file-actions">
//                                                 <button type="button" className="file-action-btn" onClick={() => DeleteSelectFile(id)}>Delete</button>
//                                             </div>
//                                         </div>
//                                     </div>
//                                 )
//                             })
//                         }
//                     </div>
//                     <div className="kb-buttons-box">
//                         <button type="submit" className="form-submit">Upload Document</button>
//                     </div>
//                 </form>
//                 {Files.length > 0 ?
//                     <div className="kb-attach-box upload-kb-attach-box">
//                         <hr />
//                         {
//                             Files.map((data, index) => {
//                                 const { id, filename, filetype, fileimage, datetime, filesize } = data;
//                                 return (
//                                     <div className="file-atc-box" key={index}>
//                                         {
//                                             filename.match(/.(jpg|jpeg|png|gif|svg)$/i) ?
//                                                 <div className="file-image"> <img src={fileimage} alt="" /></div> :
//                                                 <div className="file-image"><i className="far fa-file-alt"></i></div>
//                                         }
//                                         <div className="file-detail">
//                                             <h6>{filename}</h6>
//                                             <p><span>Size : {filesize}</span><span className="ml-3">Modified Time : {datetime}</span></p>
//                                             <div className="file-actions">
//                                                 <button className="file-action-btn" onClick={() => DeleteFile(id)}>Delete</button>
//                                                 <a href={fileimage}  className="file-action-btn" download={filename}>Download</a>
//                                             </div>
//                                         </div>
//                                     </div>
//                                 )
//                             })
//                         }
//                     </div>
//                     : ''
//                 }
//             </div>
//         </div>
//     )
// }

// export default FileUpload