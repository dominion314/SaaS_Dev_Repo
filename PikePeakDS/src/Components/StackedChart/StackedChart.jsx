import React, { Component } from 'react';
import CanvasJSReact from '@canvasjs/react-charts';
//var CanvasJSReact = require('@canvasjs/react-charts');
 
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
class App extends Component {
	constructor() {
		super();
		this.toggleDataSeries = this.toggleDataSeries.bind(this);
	}
	toggleDataSeries(e){
		if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
			e.dataSeries.visible = false;
		}
		else{
			e.dataSeries.visible = true;
		}
		this.chart.render();
	}
	render() {
		const options = {
            animationEnabled: true,
            title:{
                text: "$ 102.5M"
            },
            axisY: null,
            legend: {
                reversed: true,
                cursor: "pointer",
                fontSize: 16,
            },
            data: [
                {
                    type: "stackedColumn100",
                    name: "Silver",
                    color: "#2DDB9C",
                    dataPoints: [
                        { label: "United States", y: 897},
                        { label: "Soviet Union", y: 376},
                        { label: "Great Britain", y: 299},
                        { label: "France", y: 272},
                        { label: "Germany", y: 272},
                        { label: "Italy", y: 212},
                        { label: "Sweden", y: 210},
                        { label: "China", y: 189},
                        { label: "Russia", y: 156},
                        { label: "East Germany", y: 165}
                    ]
                },
                {
                    type: "stackedColumn100",
                    name: "Bronze",
                    color: "#6936F5",
                    dataPoints: [
                        { label: "United States", y: 789},
                        { label: "Soviet Union", y: 355},
                        { label: "Great Britain", y: 303},
                        { label: "France", y: 310},
                        { label: "Germany", y: 283},
                        { label: "Italy", y: 236},
                        { label: "Sweden", y: 233},
                        { label: "China", y: 174},
                        { label: "Russia", y: 187},
                        { label: "East Germany", y: 162}
                    ]
                }
            ]
        };
        
		return (
		<div className="chat-section">
			<CanvasJSChart options = {options}
			/>
		</div>
		);
	}
}
export default App;      