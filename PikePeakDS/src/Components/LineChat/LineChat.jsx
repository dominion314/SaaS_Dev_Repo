import React, { useEffect } from 'react';
import CanvasJSReact from '@canvasjs/react-charts';
import './LineChart.scss';

const CanvasJS = CanvasJSReact.CanvasJS;
const CanvasJSChart = CanvasJSReact.CanvasJSChart;

const LineChart = ({ data }) => {
    useEffect(() => {
        const legendItems = document.querySelectorAll('.canvasjs-chart-legend-item-symbol');
        legendItems.forEach(item => {
            item.style.width = '20px';
            item.style.height = '20px';
        });
    }, []);

    const options = {
        animationEnabled: true,
        title: {
            text: "Monthly Earnings"
        },
        axisY: {
            prefix: "",
            includeZero: false,
            labelFormatter: function(e){
                return e.value / 1000000 + "M";
            },
            gridColor: "#D9D9D9",
        },
        toolTip: {
            shared: true,
            contentFormatter: function (e) {
                let content = "";
                content += "Date: " + e.entries[0].dataPoint.label;
                return content;
            }
        },
        legend: {
            cursor: "pointer",
            itemclick: function (e) {
                if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                    e.dataSeries.visible = false;
                } else {
                    e.dataSeries.visible = true;
                }
                e.chart.render();
            },
            fontSize: 16,
            fontFamily: "Arial",
            fontWeight: "bold",
            horizontalAlign: "right",
            verticalAlign: "top",
            reversed: true,
            markerType: "none", // Hide markers on the legend
            labels: [
                { text: "Earned" },
                { text: "Forecasted" }
            ]
        },
        data: [{
            type: "spline",
            name: "Earned",
            showInLegend: true,
            lineThickness: 7,
            dataPoints: data
        },
            {
                type: "spline",
                name: "Forecasted",
                showInLegend: true,
                lineDashType: "solid",
                markerType: "none", // Markers for Forecasted
                legendMarkerType: "circle", // Marker type in the legend
                lineThickness: 7,
                lineColor: "#2DDB9C",
                dataPoints: [
                    { y: 20000000, label: "Jan" },
                    { y: 12000000, label: "Feb" },
                    { y: 32000000, label: "Mar" },
                    { y: 18000000, label: "Apr" },
                    { y: 42000000, label: "May" },
                    { y: 40000000, label: "Jun" },
                    { y: 22000000, label: "Jul" },
                    { y: 42000000, label: "Aug" }
                ]
            }
        ]
    };

    return ( 
        <div>
            <CanvasJSChart options={options} />
        </div>
    );
}

export default LineChart;
