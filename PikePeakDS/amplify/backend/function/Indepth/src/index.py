import json
import boto3
import time
import uuid
import datetime
import logging
from resources.db import Customer_RDS_Connection, get_pastmapped_data, get_parse_response_db

logger = logging.getLogger()
logger.setLevel(logging.INFO)


ecs_client = boto3.client('ecs')

def handler(event, context):
    required_keys = ['key', 'Endpoint', 'Region', 'Role_ARN', 'Secret_ID']
    current_time = datetime.datetime.now()
    
    try:
        body = json.loads(event['body'])
    except KeyError:
        logger.error("Could not pull data from request.", exc_info=True)
        response_body = {
            'message': 'Could not pull data from request.',
            'time': str(current_time),
            'id': 'unknown',
            'data': 'empty'
        }
        return {
            'statusCode': 400,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            'body': json.dumps(response_body)
            }
    
    if all(key in body for key in required_keys):
        key = body['key']
        endpoint_name = body['Endpoint']
        region = body['Region']
        role_arn = body['Role_ARN'] 
        secret_id = body['Secret_ID']

        cluster = 'BDTool'
        task_definition = 'arn:aws:ecs:us-east-2:482718065396:task-definition/umap:1'
        container_name = 'umap'
        task_id = uuid.uuid4()
        retrieval_s3_key = 'UMAP/'+str(task_id)+'_return.txt'

        try:
            cus_conn, cus_cursor = Customer_RDS_Connection(endpoint_name, role_arn, secret_id, region)
            opp_x, opp_y, opp_texts = get_parse_response_db(cus_conn, cus_cursor, key)
            pas_x, pas_y, pas_texts = get_pastmapped_data(cus_conn, cus_cursor)

            return_data = {'x_opp':opp_x, 
                'y_opp':opp_y,
                'texts_opp':opp_texts,
                'x_pas':pas_x, 
                'y_pas':pas_y,
                'texts_pas':pas_texts,
                }
            
            cus_cursor.close()
            cus_conn.close()
            


        except Exception as e:
            print(f'Error pulling data: {e}')
            return {
                'statusCode': 500,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps(f'Error pulling data: {e}')
            }

    else:
        missing_keys = [key for key in required_keys if key not in body]
        return {
                'statusCode': 400,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f"Missing required key(s): {', '.join(missing_keys)}",
                    'time': str(current_time),
                    'id': 'None',
                    'data': 'None'
                })
            }

    return {
            'statusCode': 200,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f'Data successfully retrieved',
                    'time': str(current_time),
                    'data': return_data
                })
            }




    # print('received event:')
    # print(event)

    # s3_client = boto3.client('s3')

    # # Define your S3 bucket name and the object key (path)
    # bucket_name = 'pendingdocs94117-dev'
    # object_name = 'UMAP/umap_model.pkl'

    # # Download the file from S3 into a byte stream
    # byte_stream = io.BytesIO()
    # s3_client.download_fileobj(bucket_name, object_name, byte_stream)
    # byte_stream.seek(0)  # Rewind the byte stream

    # # Load the UMAP model from the byte stream using pickle
    # umap_model = pickle.load(byte_stream)
   
    # return {
    #     'statusCode': 200,
    #     'headers': {
    #         'Access-Control-Allow-Headers': '*',
    #         'Access-Control-Allow-Origin': '*',
    #         'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
    #     },
    #     'body': json.dumps('Hello from your new Amplify Python lambda!')
    #     }