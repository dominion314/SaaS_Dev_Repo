import datetime
import logging
import json
from resources.db import get_distance_data, get_distance_data_past, Customer_RDS_Connection, Application_RDS_Connection, get_customer_rds, get_SAM_data

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def handler(event, context):
    required_keys = ['company_uuid', 'process_id', 'requested_date']
    current_time = datetime.datetime.now()
    try:
        body = json.loads(event['body'])
    except KeyError:
        logger.error("Could not pull data from request.", exc_info=True)
        response_body = {
            'message': 'Could not pull data from request.',
            'time': str(current_time),
            'id': 'unknown',
            'data': 'empty'
        }
        return {
            'statusCode': 400,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            'body': json.dumps(response_body)
            }
    
    if all(key in body for key in required_keys):
        company_uuid = body['company_uuid']
        process_id = body['process_id']
        requested_date = body['requested_date']

        try:
            app_conn, app_cursor = Application_RDS_Connection()
            endpoint_name, role_arn, secret_id, region = get_customer_rds(app_conn, app_cursor, company_uuid)

            opportunity_ids, titles, types, response_deadlines, ui_links, vectors = get_SAM_data(app_conn, app_cursor, requested_date)

            cus_conn, cus_cursor  = Customer_RDS_Connection(endpoint_name, role_arn, secret_id, region)

            counts, average, q1, q2, q3, stedv = get_distance_data(cus_conn, cus_cursor, vectors)

            average_past, stedv_past = get_distance_data_past(cus_conn, cus_cursor, vectors)

            zipped_lists = zip(opportunity_ids, titles, types, response_deadlines, ui_links, vectors, counts, average, q1, q2, q3, stedv)
            sorted_lists = sorted(zipped_lists, key=lambda x: x[7])
            opportunity_ids, titles, types, response_deadlines, ui_links, vectors, counts, average, q1, q2, q3, stedv = zip(*sorted_lists)

            opportunity_ids = list(opportunity_ids)
            titles = list(titles)
            types = list(types)
            response_deadlines = list(response_deadlines)
            ui_links = list(ui_links)
            vectors = list(vectors)
            counts = list(counts)
            average = list(average)
            q1 = list(q1)
            q2 = list(q2)
            q3 = list(q3)
            stedv = list(stedv)
            average_past = sum(average_past) / len(average_past)
            stedv_past = sum(stedv_past) / len(stedv_past)

            return_data = {'opportunity_ids':opportunity_ids, 
                           'titles':titles, 
                           'types':types, 
                           'response_deadlines':response_deadlines, 
                           'ui_links':ui_links, 
                           'counts':counts, 
                           'average':average, 
                           'q1':q1, 
                           'q2':q2, 
                           'q3':q3, 
                           'stedv':stedv,
                           'past_ave': average_past,
                           'past_stdev': stedv_past,
                           'vectors':vectors}

            response_body = {
                'message': 'Successfully retrieved.',
                'time': str(current_time),
                'id': process_id,
                'data': return_data
                }

        except Exception as e:
            logger.error(f"Failed to pull data: {e}", exc_info=True)
            response_body = {
                'message': f'Failed to retrieved: {e}',
                'time': str(current_time),
                'id': process_id,
                'data': 'empty'
            }
            return {
                'statusCode': 500,
                'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'

            },
                'body': json.dumps(response_body)
            }

    else:
        missing_keys = [key for key in required_keys if key not in body]
        response_body = {
                'message': f"Missing required key(s): {', '.join(missing_keys)}",
                'time': str(current_time),
                'id': process_id,
                'data': 'empty'
            }
        return {
                'statusCode': 400,
                'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'

            },
                'body': json.dumps(response_body)
            }

    return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'

            },
                'body': json.dumps(response_body)
            }
