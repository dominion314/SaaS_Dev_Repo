import pg8000
import ssl
import boto3
import json

def Customer_RDS_Connection(endpoint, role_arn, secret_id, region):
    sts_client = boto3.client('sts')
    assumed_role = sts_client.assume_role(
            RoleArn=role_arn,
            RoleSessionName="CrossAccountSession"
        )

    credentials = assumed_role['Credentials']
    session = boto3.Session(
            aws_access_key_id=credentials['AccessKeyId'],
            aws_secret_access_key=credentials['SecretAccessKey'],
            aws_session_token=credentials['SessionToken'],
        )

    secretsmanager = session.client('secretsmanager', region_name=region)

    secret = secretsmanager.get_secret_value(SecretId=secret_id)
    secret_dict = json.loads(secret['SecretString'])
    
    db_username = secret_dict['username']
    db_password = secret_dict['password']
    db_host = endpoint
    db_name = secret_dict['username']
    db_port = secret_dict.get('port', 5432)

    
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_NONE

    conn = pg8000.connect(
            user=db_username,
            password=db_password,
            host=db_host,
            database=db_name,
            port=db_port,
            ssl_context=ssl_context
        )
    
    cursor = conn.cursor()

    cursor.execute("SELECT EXISTS (SELECT FROM pg_type WHERE typname = 'vector');")
    exists = cursor.fetchone()[0]
    if not exists:
        cursor.execute("CREATE EXTENSION vector;")
        conn.commit()

    table_name = "documenttasks"
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        document_uuid UUID PRIMARY KEY,
        document_name TEXT,
        document_type TEXT,
        start_time TIMESTAMP,
        stop_time TIMESTAMP,
        s3_location TEXT,
        s3_delete BOOLEAN,
        status TEXT,
        return_data JSON,
        error_message TEXT
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    return conn, cursor


def add_to_task_db(conn, cursor, records):

    insert_query = """
    INSERT INTO documenttasks (document_uuid, document_name, document_type, start_time, stop_time, s3_location, s3_delete, status, return_data, error_message)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    """

    cursor.executemany(insert_query, records)

    conn.commit()
    cursor.close()
    conn.close()
