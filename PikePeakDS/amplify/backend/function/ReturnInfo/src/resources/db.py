import os
import pg8000
import ssl
import boto3
from botocore.exceptions import ClientError
import json
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def get_secret():
    secret_name = os.getenv("SECRET_NAME")
    region_name = os.getenv("SECRET_REGION")

    client = boto3.client('secretsmanager', region_name=region_name)

    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    except ClientError as e:
        raise e
        
    secret = json.loads(get_secret_value_response['SecretString'])
    return secret


def Application_RDS_Connection():
    secret = get_secret()
    
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_NONE
    
    host = os.getenv("DB_HOST")
    port = 5432 
    dbname = secret['username']
    user = secret['username']
    password = secret['password']

    conn = pg8000.connect(
        host=host,
        port=port,
        database=dbname,
        user=user,
        password=password,
        ssl_context=ssl_context)
    
    cursor = conn.cursor()
    table_name = "companyconnection"
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        company_uuid UUID PRIMARY KEY,
        company_name TEXT,
        endpoint_name TEXT,
        role_arn TEXT,
        secret_id TEXT,
        region TEXT
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    table_name = "samdata"
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name}(
            id SERIAL PRIMARY KEY,
            opportunity_id TEXT,
            posted_date TEXT,
            title TEXT,
            type TEXT,
            response_deadline TEXT,
            ui_link TEXT,
            document TEXT,
            vector_all_MiniLM_L6_v2 vector
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    return conn, cursor


def get_customer_rds(conn, cursor, company_uuid):

    insert_query = """
    SELECT endpoint_name, role_arn, secret_id, region, company_name from companyconnection WHERE company_uuid = %s;
    """

    cursor.execute(insert_query, (company_uuid,))
    rows = cursor.fetchall()

    return rows[0][0], rows[0][1], rows[0][2], rows[0][3], rows[0][4]


