import json
import boto3
import base64
import datetime
import logging

def handler(event, context):
    required_keys = ['process_id', 'role_arn', 'bucket','key']
    current_time = datetime.datetime.now()

    try:
        body = json.loads(event['body'])
    except KeyError:
        logging.error(f"No Body")
        return {
            'statusCode': 400,
            'headers': {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            'body': json.dumps({
                'message': f"Missing body in request.",
                'time': str(current_time),
                'id': 'None',
                'data': 'none'
            })
        }


    if all(key in body for key in required_keys):
        process_id = body['process_id']
        role_arn = body['role_arn']
        bucket = body['bucket']
        key = body['key']

        try:

            sts_client = boto3.client('sts')
            # assumed_role = sts_client.assume_role(
            #     RoleArn="arn:aws:iam::637423610310:role/SAMML-Application-Role",
            #     RoleSessionName="AmplifyLambdaSession"
            # )

            assumed_role = sts_client.assume_role(
                RoleArn=role_arn,
                RoleSessionName="AmplifyLambdaSession"
            )

            credentials = assumed_role['Credentials']

            s3_client = boto3.client(
                's3',
                aws_access_key_id=credentials['AccessKeyId'],
                aws_secret_access_key=credentials['SecretAccessKey'],
                aws_session_token=credentials['SessionToken']
            )

        except Exception as e:
            logging.error(f"Failed to establish S3 connection: {e}")
            return {
                'statusCode': 500,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f'Failed to establish S3 connection: {e}',
                    'time': str(current_time),
                    'id': process_id,
                    'data': 'None'
                })
            }
        
        try:
            response = s3_client.get_object(Bucket=bucket, Key=key)
            doc_data = response['Body'].read()
            encoded_data = base64.b64encode(doc_data).decode('utf-8')

            data = json.dumps({
                    'message': f'Success',
                    'time': str(current_time),
                    'id': process_id,
                    'data': encoded_data
                })

        except Exception as e:
            logging.error(f"Failed to pull document: {e}")
            return {
                'statusCode': 500,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f'Failed to establish pull document: {e}',
                    'time': str(current_time),
                    'id': process_id,
                    'data': 'None'
                })
            }
        
    else:
        missing_keys = [key for key in required_keys if key not in body]
        return {
                'statusCode': 400,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f"Missing required key(s): {', '.join(missing_keys)}",
                    'time': str(current_time),
                    'id': 'None',
                    'data': 'None'
                })
            }
    
    return {
      'statusCode': 200,
      'headers': {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
      },
      'body': data
  }