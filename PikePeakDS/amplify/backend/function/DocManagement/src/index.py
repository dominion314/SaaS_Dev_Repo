import datetime
import os
import logging
import json
from botocore.exceptions import ClientError
from resources.db import get_task_db, get_parse_db, Customer_RDS_Connection, get_parse_response_db, delete_db

def handler(event, context):
    required_keys = ['process_id', 'Endpoint', 'Region', 'Role_ARN', 'Secret_ID', 'task', 'key']
    current_time = datetime.datetime.now()
    try:
        body = json.loads(event['body'])
    except KeyError:
        logging.error(f"No Body")
        return {
            'statusCode': 400,
            'headers': {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            'body': json.dumps({
                'message': f"Missing body in request.",
                'time': str(current_time),
                'id': 'None',
                'data': 'none'
            })
        }


    if all(key in body for key in required_keys):
        process_id = body['process_id']
        endpoint_name = body['Endpoint']
        role_arn = body['Role_ARN']
        secret_id = body['Secret_ID']
        region = body['Region']
        # start = int(body['start'])
        # finish = int(body['finish'])
        task = body['task']
        key = body['key']

        try:
            cus_conn, cus_cursor = Customer_RDS_Connection(endpoint_name, role_arn, secret_id, region)

            if task == 'PWS Parse':
                document_name, process, complete, start_time, stop_time, error_message, key, percentage = get_parse_db(cus_conn, cus_cursor)
                # , start, finish)

                return_data = {'document_name':document_name, 
                    'process':process, 
                    'complete':complete, 
                    'start_time':start_time, 
                    'stop_time':stop_time, 
                    'error_message':error_message,
                    'key': key,
                    'percentage': percentage}
                
            elif task == 'Past Performance':
                document_name, process, complete, start_time, stop_time, error_message, key, percentage = get_task_db(cus_conn, cus_cursor)
                # , start, finish)

                return_data = {'document_name':document_name, 
                    'process':process, 
                    'complete':complete, 
                    'start_time':start_time, 
                    'stop_time':stop_time, 
                    'error_message':error_message,
                    'key': key,
                    'percentage': percentage}
                
            
            elif task == 'Retrieve':
                return_data = get_parse_response_db(cus_conn, cus_cursor, key)
                
            elif task == 'Delete':
                delete_db(cus_conn, cus_cursor, key)
                return {
                    'statusCode': 200,
                        'headers': {
                            'Access-Control-Allow-Headers': '*',
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                        },
                        'body': json.dumps({
                            'message': f'Data successfully deleted',
                            'time': str(current_time),
                            'id': process_id,
                            'data': ''
                        })
                    }
            else:
                return {
                'statusCode': 400,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f"Improper Task.",
                    'time': str(current_time),
                    'id': 'None',
                    'data': 'None'
                })
            }


        except Exception as e:
            logging.error(f"Failed to pull from database: {e}")
            return {
                'statusCode': 500,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f'Failed to add task to database and files are deleted: {e}',
                    'time': str(current_time),
                    'id': process_id,
                    'data': 'None'
                })
            }

    else:
        missing_keys = [key for key in required_keys if key not in body]
        return {
                'statusCode': 400,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f"Missing required key(s): {', '.join(missing_keys)}",
                    'time': str(current_time),
                    'id': 'None',
                    'data': 'None'
                })
            }

    return {
            'statusCode': 200,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f'Data successfully retrieved',
                    'time': str(current_time),
                    'id': process_id,
                    'data': return_data
                })
            }