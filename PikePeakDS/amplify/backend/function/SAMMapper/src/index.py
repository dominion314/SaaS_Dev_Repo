import datetime
import logging
import json
from resources.db import Customer_RDS_Connection, Application_RDS_Connection, get_customer_rds, get_SAM_data, get_cus_data, pca_data

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def handler(event, context):
    required_keys = ['company_uuid', 'document_meta', 'process_id', 'requested_date', 'SAM_ID']
    current_time = datetime.datetime.now()
    try:
        body = json.loads(event['body'])
    except KeyError:
        logger.error("Could not pull data from request.", exc_info=True)
        response_body = {
            'message': 'Could not pull data from request.',
            'time': str(current_time),
            'id': 'unknown',
            'data': 'empty'
        }
        return {
            'statusCode': 400,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            'body': json.dumps(response_body)
            }
    
    if all(key in body for key in required_keys):
        company_uuid = body['company_uuid']
        SAM_ID = body['SAM_ID']
        process_id = body['process_id']
        requested_date = body['requested_date']

        try:
            app_conn, app_cursor = Application_RDS_Connection()
            endpoint_name, role_arn, secret_id, region = get_customer_rds(app_conn, app_cursor, company_uuid)

            SAM_vector = get_SAM_data(app_conn, app_cursor, requested_date, SAM_ID)

            cus_conn, cus_cursor  = Customer_RDS_Connection(endpoint_name, role_arn, secret_id, region)

            cus_vector = get_cus_data(cus_conn, cus_cursor)

            labels = ['sam']*len(SAM_vector)+['cus']*len(cus_vector)

            x, y = pca_data(cus_conn, cus_cursor)

            x = list(x)
            y = list(y)
            labels = list(labels)

            return_data = {'x':x, 
                           'y':y, 
                           'labels':labels}

            response_body = {
                'message': 'Successfully retrieved.',
                'time': str(current_time),
                'id': process_id,
                'data': return_data
                }

        except Exception as e:
            logger.error("Failed to pull data: {e}", exc_info=True)
            response_body = {
                'message': 'Failed to retrieved: {e}',
                'time': str(current_time),
                'id': process_id,
                'data': 'empty'
            }
            return {
                'statusCode': 500,
                'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'

            },
                'body': json.dumps(response_body)
            }

    else:
        missing_keys = [key for key in required_keys if key not in body]
        response_body = {
                'message': f"Missing required key(s): {', '.join(missing_keys)}",
                'time': str(current_time),
                'id': process_id,
                'data': 'empty'
            }
        return {
                'statusCode': 400,
                'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'

            },
                'body': json.dumps(response_body)
            }

    return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'

            },
                'body': json.dumps(response_body)
            }
