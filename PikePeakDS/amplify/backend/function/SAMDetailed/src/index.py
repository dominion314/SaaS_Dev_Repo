import json
import datetime
import logging
from resources.db import Customer_RDS_Connection, get_distance_data

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def handler(event, context):
    required_keys = ['Endpoint','Region','Role_ARN','Secret_ID', 'Vector']
    current_time = datetime.datetime.now()
    
    try:
        body = json.loads(event['body'])
    except KeyError:
        logger.error("Could not pull data from request.", exc_info=True)
        response_body = {
            'message': 'Could not pull data from request.',
            'time': str(current_time),
            'id': 'unknown',
            'data': 'empty'
        }
        return {
            'statusCode': 400,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            'body': json.dumps(response_body)
            }
    
    if all(key in body for key in required_keys):
        try: 
            endpoint_name = body['Endpoint']
            role_arn = body['Role_ARN']
            secret_id = body['Secret_ID']
            region = body['Region']
            vector = body['Vector']

            cus_conn, cus_cursor  = Customer_RDS_Connection(endpoint_name, role_arn, secret_id, region)

            similarities, metas = get_distance_data(cus_conn, cus_cursor, vector)

            similarities = list(similarities)
            metas = list(metas)

            return_data = {'similarities':similarities, 
                            'metas':metas}

            response_body = {
                'message': 'Successfully retrieved.',
                'time': str(current_time),
                'data': return_data
                }

        except Exception as e:
            logger.error("Failed to pull data: {e}", exc_info=True)
            response_body = {
                'message': 'Failed to retrieved: {e}',
                'time': str(current_time),
                'data': 'empty'
            }
            return {
                'statusCode': 500,
                'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'

            },
                'body': json.dumps(response_body)
            }

    else:
        missing_keys = [key for key in required_keys if key not in body]
        response_body = {
                'message': f"Missing required key(s): {', '.join(missing_keys)}",
                'time': str(current_time),
                'data': 'empty'
            }
        return {
                'statusCode': 400,
                'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'

            },
                'body': json.dumps(response_body)
            }

    return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'

            },
                'body': json.dumps(response_body)
            }
