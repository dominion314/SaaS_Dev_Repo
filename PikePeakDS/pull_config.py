import boto3
import json

# Initialize ECS client
ecs_client = boto3.client('ecs', region_name='us-east-2')

# List clusters
clusters = ecs_client.list_clusters()['clusterArns']
print("Clusters:", clusters)

# Describe each cluster
for cluster in clusters:
    cluster_info = ecs_client.describe_clusters(clusters=[cluster])
    print("Cluster Info:", json.dumps(cluster_info, indent=4))

    # List services in the cluster
    services = ecs_client.list_services(cluster=cluster)['serviceArns']
    print("Services:", services)

    # Describe each service
    for service in services:
        service_info = ecs_client.describe_services(cluster=cluster, services=[service])
        print("Service Info:", json.dumps(service_info, indent=4))

        # Describe task definition
        task_definition = service_info['services'][0]['taskDefinition']
        task_info = ecs_client.describe_task_definition(taskDefinition=task_definition)
        print("Task Definition Info:", json.dumps(task_info, indent=4))