import boto3
import json
from datetime import datetime

# Custom JSON serializer for datetime objects
def json_serial(obj):
    if isinstance(obj, datetime):
        return obj.isoformat()  # Convert datetime to ISO format
    raise TypeError("Type not serializable")

# Initialize SageMaker client
sagemaker_client = boto3.client('sagemaker', region_name='us-east-2')

# List models
models = sagemaker_client.list_models()
print("Models:", json.dumps(models, default=json_serial, indent=4))

# Describe each model
for model in models['Models']:
    model_info = sagemaker_client.describe_model(ModelName=model['ModelName'])
    print("Model Info:", json.dumps(model_info, default=json_serial, indent=4))

# List endpoints
endpoints = sagemaker_client.list_endpoints()
print("Endpoints:", json.dumps(endpoints, default=json_serial, indent=4))

# Describe each endpoint
for endpoint in endpoints['Endpoints']:
    endpoint_info = sagemaker_client.describe_endpoint(EndpointName=endpoint['EndpointName'])
    print("Endpoint Info:", json.dumps(endpoint_info, default=json_serial, indent=4))