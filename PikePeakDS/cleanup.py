import boto3
import json
import time

def clean_parameter_list(filename):
    """Clean the parameter list file to get one parameter per line"""
    with open(filename, 'r') as f:
        # Read all lines and split on whitespace
        content = f.read()
        # Split on whitespace and filter out empty strings
        parameters = [p.strip() for p in content.split() if p.strip()]
    
    # Write back clean parameter list
    with open('clean_parameters.txt', 'w') as f:
        for param in parameters:
            f.write(f"{param}\n")
    
    return parameters

def get_parameter_value(ssm_client, parameter_name):
    """Get parameter value with error handling"""
    try:
        response = ssm_client.get_parameter(
            Name=parameter_name,
            WithDecryption=True
        )
        return {
            'name': parameter_name,
            'value': response['Parameter']['Value'],
            'type': response['Parameter']['Type']
        }
    except Exception as e:
        print(f"Error getting parameter {parameter_name}: {e}")
        return None

def main():
    # Configure AWS client
    session = boto3.Session(profile_name='param-migrate', region_name='us-east-2')
    ssm = session.client('ssm')
    
    # Clean and read parameter names
    parameters = clean_parameter_list('parameters_list.txt')
    print(f"Found {len(parameters)} unique parameters")
    
    # Get values for each parameter
    parameter_data = []
    for param_name in parameters:
        print(f"Getting value for: {param_name}")
        param_info = get_parameter_value(ssm, param_name)
        if param_info:
            parameter_data.append(param_info)
        time.sleep(0.1)  # Small delay to avoid API throttling
    
    # Save to JSON file
    with open('parameter_values.json', 'w') as f:
        json.dump(parameter_data, f, indent=2)
    
    print(f"\nSuccessfully exported {len(parameter_data)} parameters to parameter_values.json")
    print(f"Failed to export {len(parameters) - len(parameter_data)} parameters")

if __name__ == "__main__":
    main()
