const express = require('express');
const cors = require('cors');
const multer = require('multer');
const { spawn } = require('child_process');

const app = express();
app.use(cors());

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

app.post('/optimize', upload.single('resume'), (req, res) => {
  if (!req.file) {
    return res.status(400).send('No resume file provided');
  }

  const pythonProcess = spawn('python', ['process_resume.py']);
  pythonProcess.stdin.write(req.file.buffer);

  let result = '';
  pythonProcess.stdout.on('data', (data) => {
    result += data.toString();
  });

  pythonProcess.stderr.on('data', (data) => {
    console.error(`stderr: ${data}`);
  });

  pythonProcess.on('close', (code) => {
    if (code !== 0) {
      return res.status(500).send('Error processing resume');
    }
    res.json(JSON.parse(result));
  });

  pythonProcess.stdin.end();
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
