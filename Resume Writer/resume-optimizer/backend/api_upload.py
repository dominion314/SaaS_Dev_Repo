from flask import Flask, request
import json

app = Flask(__name__)

@app.route('/optimize', methods=['POST'])
def optimize():
    occupation = request.form['occupation']
    resume_file = request.files['resume']
    # Process the resume file, optimize and return the result
    # ...

if __name__ == '__main__':
    app.run()
