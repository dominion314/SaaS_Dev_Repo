import React, { useState } from 'react';
import './App.css';

function App() {
  const [selectedFile, setSelectedFile] = useState(null);

  const handleFileChange = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const handleButtonClick = () => {
    uploadResume();
  };

  async function uploadResume() {
    if (!selectedFile) {
      alert('Please select a resume file to analyze.');
      return;
    }

    const formData = new FormData();
    formData.append('resume', selectedFile);

    try {
      const apiUrl = process.env.REACT_APP_API_URL || 'http://localhost:3001';
      const response = await fetch(`${apiUrl}/optimize`, {
        method: 'POST',
        body: formData,
      });

      if (response.ok) {
        const result = await response.json();
        // Display the result to the user
        console.log('Optimized resume:', result);
      } else {
        // Handle any errors from the API
        console.error('Error analyzing resume:', response.statusText);
      }
    } catch (error) {
      // Handle any network errors
      console.error('Error uploading resume:', error);
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <h1>Automated Resume Review and Optimization</h1>
        <input
          type="file"
          id="resumeInput"
          accept=".pdf,.doc,.docx"
          onChange={handleFileChange}
        />
        <button onClick={handleButtonClick}>Analyze Resume</button>
      </header>
    </div>
  );
}

export default App;
