import os
import pg8000
import ssl
import boto3
from botocore.exceptions import ClientError
import json

env_name = os.getenv("ENV")
if env_name == 'dev':
    addition = '_dev'
else:
    addition = ''

def get_secret():
    secret_name = os.getenv("SECRET_NAME")
    region_name = os.getenv("SECRET_REGION")

    client = boto3.client('secretsmanager', region_name=region_name)

    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    except ClientError as e:
        raise e
        
    secret = json.loads(get_secret_value_response['SecretString'])
    return secret


def Customer_RDS_Connection(endpoint, role_arn, secret_id, region):
    sts_client = boto3.client('sts')
    assumed_role = sts_client.assume_role(
            RoleArn=role_arn,
            RoleSessionName="CrossAccountSession"
        )

    credentials = assumed_role['Credentials']
    session = boto3.Session(
            aws_access_key_id=credentials['AccessKeyId'],
            aws_secret_access_key=credentials['SecretAccessKey'],
            aws_session_token=credentials['SessionToken'],
        )

    secretsmanager = session.client('secretsmanager', region_name=region)

    secret = secretsmanager.get_secret_value(SecretId=secret_id)
    secret_dict = json.loads(secret['SecretString'])
    
    db_username = secret_dict['username']
    db_password = secret_dict['password']
    db_host = endpoint
    db_name = secret_dict['username']
    db_port = secret_dict.get('port', 5432)

    
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_NONE

    conn = pg8000.connect(
            user=db_username,
            password=db_password,
            host=db_host,
            database=db_name,
            port=db_port,
            ssl_context=ssl_context
        )
    
    cursor = conn.cursor()

    cursor.execute("SELECT EXISTS (SELECT FROM pg_type WHERE typname = 'vector');")
    exists = cursor.fetchone()[0]
    if not exists:
        cursor.execute("CREATE EXTENSION vector;")
        conn.commit()

    table_name = "documenttasks"+addition
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        document_uuid UUID PRIMARY KEY,
        document_name TEXT,
        document_type TEXT,
        start_time TIMESTAMP,
        stop_time TIMESTAMP,
        s3_location TEXT,
        s3_delete BOOLEAN,
        status TEXT,
        return_data JSON,
        error_message TEXT,
        percentage NUMERIC(5, 2)
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    return conn, cursor


def get_task_db(conn, cursor):
    # , start, finish):
    # limit = finish - start
    # offset = start


    query = """
    SELECT document_name, document_type, status, start_time, stop_time, error_message, document_uuid, percentage
    FROM documenttasks"""+addition+""" 
    WHERE document_type != 'PWS Parse'
    ORDER BY start_time DESC;
    """

    cursor.execute(query,)
                    # (limit, offset))
    rows = cursor.fetchall()

    document_name = []
    process = []
    complete = []
    start_time = []
    stop_time = []
    error_message = []
    key = []
    percentage = []

    for row in rows:
        document_name.append(row[0])
        process.append(row[1])
        complete.append(row[2])
        start_time.append(str(row[3]))
        stop_time.append(str(row[4]))
        error_message.append(row[5])
        key.append(str(row[6]))
        percentage.append(str(row[7]))

    cursor.close()
    conn.close()

    return document_name, process, complete, start_time, stop_time, error_message, key, percentage


def get_parse_db(conn, cursor):
                #  , start, finish):
    # limit = finish - start
    # offset = start

    query = """
    SELECT document_name, document_type, status, start_time, stop_time, error_message, document_uuid, percentage
    FROM documenttasks"""+addition+""" 
    WHERE document_type = 'PWS Parse'
    ORDER BY start_time DESC;"""
    # LIMIT %s OFFSET %s;
    # """

    cursor.execute(query, )
    rows = cursor.fetchall()

    document_name = []
    process = []
    complete = []
    start_time = []
    stop_time = []
    error_message = []
    key = []
    percentage = []

    for row in rows:
        document_name.append(row[0])
        process.append(row[1])
        complete.append(row[2])
        start_time.append(str(row[3]))
        stop_time.append(str(row[4]))
        error_message.append(row[5])
        key.append(str(row[6]))
        percentage.append(str(row[7]))

    cursor.close()
    conn.close()

    return document_name, process, complete, start_time, stop_time, error_message, key, percentage


def get_parse_response_db(conn, cursor, key):
    query = """
    SELECT return_data
    FROM documenttasks"""+addition+""" 
    WHERE document_uuid = %s
    """

    cursor.execute(query, (key, ))
    row = cursor.fetchone()

    cursor.close()
    conn.close()

    return row

def delete_db(conn, cursor, key):
    query = """
    DELETE
    FROM documenttasks"""+addition+""" 
    WHERE document_uuid = %s
    """

    cursor.execute(query, (key, ))
    conn.commit()

    query = """
    DELETE
    FROM pastperformance"""+addition+""" 
    WHERE document_uuid = %s
    """

    cursor.execute(query, (key, ))
    conn.commit()

    cursor.close()
    conn.close()


def get_customer_rds(secret_name, region_name):
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )
    get_secret_value_response = client.get_secret_value(
        SecretId=secret_name
    )
    secret = get_secret_value_response['SecretString']
    values = json.loads(secret)
    return values['endpoint'], values['role_arn'], values['secret_id'], values['region']
