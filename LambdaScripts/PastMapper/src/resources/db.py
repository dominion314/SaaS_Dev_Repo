import os
import pg8000
import ssl
import boto3
from botocore.exceptions import ClientError
import json

env_name = os.getenv("ENV")
if env_name == 'dev':
    addition = '_dev'
else:
    addition = ''

def get_secret():
    secret_name = os.getenv("SECRET_NAME")
    region_name = os.getenv("SECRET_REGION")

    client = boto3.client('secretsmanager', region_name=region_name)

    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    except ClientError as e:
        raise e
        
    secret = json.loads(get_secret_value_response['SecretString'])
    return secret


def Application_RDS_Connection():
    secret = get_secret()
    
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_NONE
    
    host = os.getenv("DB_HOST")
    port = 5432 
    dbname = secret['username']
    user = secret['username']
    password = secret['password']

    conn = pg8000.connect(
        host=host,
        port=port,
        database=dbname,
        user=user,
        password=password,
        ssl_context=ssl_context)
    
    cursor = conn.cursor()
    table_name = "companyconnection"
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        company_uuid UUID PRIMARY KEY,
        company_name TEXT,
        endpoint_name TEXT,
        role_arn TEXT,
        secret_id TEXT,
        region TEXT
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    table_name = "samdata"
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name}(
            id SERIAL PRIMARY KEY,
            opportunity_id TEXT,
            posted_date TEXT,
            title TEXT,
            type TEXT,
            response_deadline TEXT,
            ui_link TEXT,
            document TEXT,
            vector_all_MiniLM_L6_v2 vector
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    return conn, cursor

def Customer_RDS_Connection(endpoint, role_arn, secret_id, region):
    sts_client = boto3.client('sts')
    assumed_role = sts_client.assume_role(
            RoleArn=role_arn,
            RoleSessionName="CrossAccountSession"
        )

    credentials = assumed_role['Credentials']
    session = boto3.Session(
            aws_access_key_id=credentials['AccessKeyId'],
            aws_secret_access_key=credentials['SecretAccessKey'],
            aws_session_token=credentials['SessionToken'],
        )

    secretsmanager = session.client('secretsmanager', region_name=region)

    secret = secretsmanager.get_secret_value(SecretId=secret_id)
    secret_dict = json.loads(secret['SecretString'])
    
    db_username = secret_dict['username']
    db_password = secret_dict['password']
    db_host = endpoint
    db_name = secret_dict['username']
    db_port = secret_dict.get('port', 5432)

    
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_NONE

    conn = pg8000.connect(
            user=db_username,
            password=db_password,
            host=db_host,
            database=db_name,
            port=db_port,
            ssl_context=ssl_context
        )
    
    cursor = conn.cursor()

    cursor.execute("SELECT EXISTS (SELECT FROM pg_type WHERE typname = 'vector');")
    exists = cursor.fetchone()[0]
    if not exists:
        cursor.execute("CREATE EXTENSION vector;")
        conn.commit()

    table_name = "documenttasks"+addition
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        task_uuid UUID PRIMARY KEY,
        document_name TEXT,
        document_type TEXT,
        start_time TIMESTAMP,
        stop_time TIMESTAMP,
        process TEXT,
        s3_location TEXT,
        complete BOOLEAN,
        return_data JSON,
        error_message TEXT
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

        table_name = "pastperformance"+addition
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        document_uuid UUID,
        segment_uuid UUID PRIMARY KEY,
        metadata JSON,
        text TEXT,
        base_vector vector
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    table_name = "previouscontracts"+addition
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        document_uuid UUID,
        segment_uuid UUID PRIMARY KEY,
        metadata JSON,
        text TEXT,
        base_vector vector
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    return conn, cursor

# def extract_features(metadatas):
#     x_dimensions = []
#     y_dimensions = []
#     topics = []
#     names = []
#     organizations = []

#     for metadata in metadatas:

#         if metadata is None:
#             names.append(None)
#             organizations.append(None)
#             x_dimensions.append(None)
#             y_dimensions.append(None)
#             topics.append(None)

#         elif metadata is not None:
#             names.append(metadata.get('name', None))
#             organizations.append(metadata.get('organization', None))

#             if 'x_dimension' in metadata and 'y_dimension' in metadata:
#                 try:
#                     x = float(metadata['x_dimension'])
#                     y = float(metadata['y_dimension'])
#                     x_dimensions.append(x)
#                     y_dimensions.append(y)
#                 except ValueError:
#                     x_dimensions.append(None)
#                     y_dimensions.append(None)
#             else:
#                 x_dimensions.append(None)
#                 y_dimensions.append(None)

#             topics.append(metadata.get('topic', None))
        

#     return names, organizations, x_dimensions, y_dimensions, topics

# def get_pastmapped_data(conn, cursor):  
#     insert_query = """ SELECT metadata, text FROM pastperformance;"""

#     cursor.execute(insert_query)
#     rows = cursor.fetchall()

#     metadatas = [row[0] for row in rows]
#     texts = [row[1] for row in rows]

#     names, org, x_dims, y_dims, topics = extract_features(metadatas)

#     return names, org, x_dims, y_dims, topics, texts

def get_pastmapped_data(conn, cursor):  
    select_query = """SELECT 
                        metadata->>'name' AS name,
                        metadata->>'organization' AS organization,
                        CASE
                            WHEN metadata::jsonb ? 'x_dimension' AND metadata::jsonb ? 'y_dimension' THEN CAST(metadata->>'x_dimension' AS FLOAT)
                            ELSE NULL
                        END AS x_dimension,
                        CASE
                            WHEN metadata::jsonb ? 'x_dimension' AND metadata::jsonb ? 'y_dimension' THEN CAST(metadata->>'y_dimension' AS FLOAT)
                            ELSE NULL
                        END AS y_dimension,
                        metadata->>'topic' AS topic,
                        metadata->>'key' AS key,
                        metadata->>'page' AS page,
                        text
                    FROM 
                        pastperformance"""+addition+""" 
                        
                    WHERE
                        document_type = 'Past Performance';"""

    cursor.execute(select_query)
    rows = cursor.fetchall()

    names = [row[0] for row in rows]
    organizations = [row[1] for row in rows]
    x_dimensions = [row[2] for row in rows]
    y_dimensions = [row[3] for row in rows]
    topics = [row[4] for row in rows]
    keys = [row[5] for row in rows]
    pages = [row[6] for row in rows]
    texts = [row[7] for row in rows]

    return names, organizations, x_dimensions, y_dimensions, topics, keys, pages, texts

# def get_pastmapped_data(conn, cursor):  
#     select_query = """SELECT text, metadata
#                 FROM pastperformance
#                 where document_type = 'Past Performance';"""

#     cursor.execute(select_query)
#     rows = cursor.fetchall()

#     texts = [row[0] for row in rows]
#     metadatas = [row[1] for row in rows]


#     return metadatas, texts




def get_customer_rds(secret_name, region_name):
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )
    get_secret_value_response = client.get_secret_value(
        SecretId=secret_name
    )
    secret = get_secret_value_response['SecretString']
    values = json.loads(secret)
    return values['endpoint'], values['role_arn'], values['secret_id'], values['region']