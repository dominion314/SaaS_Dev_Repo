import datetime
import logging
import json
import os
from resources.db import Customer_RDS_Connection, get_pastmapped_data, get_customer_rds

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def handler(event, context):
    required_keys = ['process_id', 'Secret_Name']
    current_time = datetime.datetime.now()
    try:
        body = json.loads(event['body'])
    except KeyError:
        logger.error("Could not pull data from request.", exc_info=True)
        response_body = {
            'message': 'Could not pull data from request.',
            'time': str(current_time),
            'id': 'unknown',
            'data': 'empty'
        }
        return {
            'statusCode': 400,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            'body': json.dumps(response_body)
            }
    
    if all(key in body for key in required_keys):
        process_id = body['process_id']
        secret_name = body['Secret_Name']
        secret_region = os.getenv("SECRET_REGION")

        try:
            endpoint_name, role_arn, secret_id, region = get_customer_rds(secret_name, secret_region)
            cus_conn, cus_cursor = Customer_RDS_Connection(endpoint_name, role_arn, secret_id, region)
            names, org, x_dims, y_dims, topics, keys, pages, texts = get_pastmapped_data(cus_conn, cus_cursor)

            return_data = {'x':x_dims, 
                           'y':y_dims,
                           'names':names,
                           'orgs':org,
                           'topics':topics,
                           'keys':keys,
                           'pages':pages,
                           'labels':texts
                           }
            
            response_body = {
                'message': 'Successfully retrieved.',
                'time': str(current_time),
                'id': process_id,
                'data': return_data
                }

        except Exception as e:
            logger.error(f"Failed to pull data: {e}", exc_info=True)
            response_body = {
                'message': f'Failed to retrieved: {e}',
                'time': str(current_time),
                'id': process_id,
                'data': 'empty'
            }
            return {
                'statusCode': 500,
                'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'

            },
                'body': json.dumps(response_body)
            }

    else:
        missing_keys = [key for key in required_keys if key not in body]
        response_body = {
                'message': f"Missing required key(s): {', '.join(missing_keys)}",
                'time': str(current_time),
                'data': 'empty'
            }
        return {
                'statusCode': 400,
                'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'

            },
                'body': json.dumps(response_body)
            }

    return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
                'body': json.dumps(response_body)
            }