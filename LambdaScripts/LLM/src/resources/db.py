import pg8000
import ssl
import boto3
from botocore.exceptions import ClientError
import json
import os

env_name = os.getenv("ENV")
if env_name == 'dev':
    addition = '_dev'
else:
    addition = ''

def Customer_RDS_Connection(endpoint, role_arn, secret_id, region):
    sts_client = boto3.client('sts')
    assumed_role = sts_client.assume_role(
            RoleArn=role_arn,
            RoleSessionName="CrossAccountSession"
        )

    credentials = assumed_role['Credentials']
    session = boto3.Session(
            aws_access_key_id=credentials['AccessKeyId'],
            aws_secret_access_key=credentials['SecretAccessKey'],
            aws_session_token=credentials['SessionToken'],
        )

    secretsmanager = session.client('secretsmanager', region_name=region)

    secret = secretsmanager.get_secret_value(SecretId=secret_id)
    secret_dict = json.loads(secret['SecretString'])
    
    db_username = secret_dict['username']
    db_password = secret_dict['password']
    db_host = endpoint
    db_name = secret_dict['username']
    db_port = secret_dict.get('port', 5432)

    
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_NONE

    conn = pg8000.connect(
            user=db_username,
            password=db_password,
            host=db_host,
            database=db_name,
            port=db_port,
            ssl_context=ssl_context
        )
    
    cursor = conn.cursor()

    cursor.execute("SELECT EXISTS (SELECT FROM pg_type WHERE typname = 'vector');")
    exists = cursor.fetchone()[0]
    if not exists:
        cursor.execute("CREATE EXTENSION vector;")
        conn.commit()

    table_name = "pastperformance"+addition
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        document_uuid UUID,
        segment_uuid UUID PRIMARY KEY,
        metadata JSON,
        text TEXT,
        base_vector vector
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    table_name = "previouscontracts"+addition
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        document_uuid UUID,
        segment_uuid UUID PRIMARY KEY,
        metadata JSON,
        text TEXT,
        base_vector vector
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    return conn, cursor

def get_RAG_data(conn, cursor, input_vector):  

    vector_str = '[' + ','.join(map(str, input_vector)) + ']'

    query = """
    SELECT metadata, text 
    FROM pastperformance"""+addition+""" 
    ORDER BY base_vector <=> %s
    LIMIT 10;
    """

    cursor.execute(query, (vector_str,))

    rows = cursor.fetchall()

    metadatas = [row[0] for row in rows]
    texts = [row[1] for row in rows]


    return metadatas, texts

def get_customer_rds(secret_name, region_name):
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )
    get_secret_value_response = client.get_secret_value(
        SecretId=secret_name
    )
    secret = get_secret_value_response['SecretString']
    values = json.loads(secret)
    return values['endpoint'], values['role_arn'], values['secret_id'], values['region']
