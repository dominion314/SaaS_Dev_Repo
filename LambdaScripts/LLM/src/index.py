import json
import boto3
import datetime
import logging
import os
from resources.db import Customer_RDS_Connection, get_RAG_data, get_customer_rds
from resources.LLM import vectorize
import requests

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def handler(event, context):

    required_keys = ['secret_name', 'Prompt', 'Company_Name']
    current_time = datetime.datetime.now()

    try:
        body = json.loads(event['body'])
        # process_id = body['process_id']
    except KeyError:
        logger.error("Could not pull data from request.", exc_info=True)
        response_body = {
            'message': 'Could not pull data from request.',
            'time': str(current_time),
            'id': 'unknown',
            'data': 'empty'
        }
        return {
            'statusCode': 400,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            'body': json.dumps(response_body)
            }
    
     
    if all(key in body for key in required_keys):
        secret_name = body['secret_name']
        secret_region = os.getenv("SECRET_REGION")
        prompt = body['Prompt']
        company_name = body['Company_Name']

        try:
            endpoint_name, role_arn, secret_id, region = get_customer_rds(secret_name, secret_region)
            cus_conn, cus_cursor = Customer_RDS_Connection(endpoint_name, role_arn, secret_id, region)
            vector = vectorize([prompt])
            Metadatas, Context = get_RAG_data(cus_conn, cus_cursor, vector)

            openai_api_key = os.getenv("OPENAI_API_KEY")

            url = "https://api.openai.com/v1/chat/completions"
            headers = {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': f'Bearer {openai_api_key}'
            }

            data = {
                "model": "gpt-4",
                "messages": [
                    {"role": "system", "content": f"You are an assistant to the Business Development Group in a US Federal Government contractor company Named {company_name}. You are going to assist the organization by sumarizing past performance."},
                    {"role": "user", "content": f"The user asks the following question: \n{prompt} \n Please use this information to respond: {Context} \n If there are no relevant entries just let the user know that."},
                ]
            }

            response = requests.post(url, headers=headers, json=data)


            # region_name_llm = os.getenv("REGION_NAME_LLM")
            # endpoint_name_llm = os.getenv("ENDPOINT_NAME_LLM")
            # inference_component_name_llm = os.getenv("INFERENCE_NAME_LLM")


            # client = boto3.client('sagemaker-runtime', region_name=region_name_llm)

            # payload = {
            #     "inputs": "You are an assistant to the Business Development Group in a US Federal Government "+
            #     f"contractor company Named {company_name}. You are going to assist the organization by sumarizing past performance."+
            #     f" The user asks the following question: \n{prompt} \n Please use this information to respond: {Context} \n If there are no relevant entries just let the user know that.",
            #     "parameters": {
            #         "max_new_tokens": 256,
            #         "top_p": 0.9,
            #         "temperature": 0.6
            #     }
            # }

            # response = client.invoke_endpoint(
            #     EndpointName=endpoint_name_llm,
            #     ContentType='application/json',
            #     Body=json.dumps(payload),
            #     InferenceComponentName=inference_component_name_llm
            # )

            # response = json.loads(response['Body'].read().decode())


            # return_data = {
            #     'Response':response[0]['generated_text'].split("']")[-1], 
            #     'Metadatas':Metadatas,
            #     }

            return_data = {
                'Response':response.json()['choices'][0]['message']['content'], 
                'Metadatas':Metadatas,
                }
            
            response_body = {
                'message': 'Successfully retrieved.',
                'time': str(current_time),
                # 'id': process_id,
                'data': return_data
                }
            

        except Exception as e:
            return {
                'statusCode': 500,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({'error': str(e)})
            }
        
    else:
        missing_keys = [key for key in required_keys if key not in body]
        response_body = {
                'message': f"Missing required key(s): {', '.join(missing_keys)}",
                'time': str(current_time),
                # 'id': process_id,
                'data': 'empty'
            }
        return {
                'statusCode': 400,
                'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'

            },
                'body': json.dumps(response_body)
            }
    
    return {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
        },
        'body': json.dumps(response_body)
    }
