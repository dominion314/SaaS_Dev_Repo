import os
import pg8000
import ssl
import boto3
from botocore.exceptions import ClientError
import json

env_name = os.getenv("ENV")
if env_name == 'dev':
    addition = '_dev'
else:
    addition = ''

def get_secret():
    secret_name = os.getenv("SECRET_NAME")
    region_name = os.getenv("SECRET_REGION")

    client = boto3.client('secretsmanager', region_name=region_name)

    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    except ClientError as e:
        raise e
        
    secret = json.loads(get_secret_value_response['SecretString'])
    return secret


def Application_RDS_Connection():
    secret = get_secret()
    
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_NONE
    
    host = os.getenv("DB_HOST")
    port = 5432 
    dbname = secret['username']
    user = secret['username']
    password = secret['password']

    conn = pg8000.connect(
        host=host,
        port=port,
        database=dbname,
        user=user,
        password=password,
        ssl_context=ssl_context)
    
    cursor = conn.cursor()
    table_name = "companyconnection"
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        company_uuid UUID PRIMARY KEY,
        company_name TEXT,
        endpoint_name TEXT,
        role_arn TEXT,
        secret_id TEXT,
        region TEXT
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    table_name = "samdata"
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name}(
            id SERIAL PRIMARY KEY,
            opportunity_id TEXT,
            posted_date TEXT,
            title TEXT,
            type TEXT,
            response_deadline TEXT,
            ui_link TEXT,
            document TEXT,
            vector_all_MiniLM_L6_v2 vector
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    return conn, cursor

def Customer_RDS_Connection(endpoint, role_arn, secret_id, region):
    sts_client = boto3.client('sts')
    assumed_role = sts_client.assume_role(
            RoleArn=role_arn,
            RoleSessionName="CrossAccountSession"
        )

    credentials = assumed_role['Credentials']
    session = boto3.Session(
            aws_access_key_id=credentials['AccessKeyId'],
            aws_secret_access_key=credentials['SecretAccessKey'],
            aws_session_token=credentials['SessionToken'],
        )

    secretsmanager = session.client('secretsmanager', region_name=region)

    secret = secretsmanager.get_secret_value(SecretId=secret_id)
    secret_dict = json.loads(secret['SecretString'])
    
    db_username = secret_dict['username']
    db_password = secret_dict['password']
    db_host = endpoint
    db_name = secret_dict['username']
    db_port = secret_dict.get('port', 5432)

    
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_NONE

    conn = pg8000.connect(
            user=db_username,
            password=db_password,
            host=db_host,
            database=db_name,
            port=db_port,
            ssl_context=ssl_context
        )
    
    cursor = conn.cursor()

    cursor.execute("SELECT EXISTS (SELECT FROM pg_type WHERE typname = 'vector');")
    exists = cursor.fetchone()[0]
    if not exists:
        cursor.execute("CREATE EXTENSION vector;")
        conn.commit()

    table_name = "documenttasks"+addition
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        document_uuid UUID PRIMARY KEY,
        document_name TEXT,
        document_type TEXT,
        start_time TIMESTAMP,
        stop_time TIMESTAMP,
        s3_location TEXT,
        s3_delete BOOLEAN,
        status TEXT,
        return_data JSON,
        error_message TEXT,
        percentage NUMERIC(5, 2)
        );
        """
        cursor.execute(create_table_query)
        conn.commit()


    table_name = "pastperformance"+addition
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        document_uuid UUID,
        segment_uuid UUID PRIMARY KEY,
        metadata JSON,
        text TEXT,
        base_vector vector
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    table_name = "previouscontracts"+addition
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = f"""
        CREATE TABLE {table_name} (
        document_uuid UUID,
        segment_uuid UUID PRIMARY KEY,
        metadata JSON,
        text TEXT,
        base_vector vector
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    return conn, cursor

# def get_customer_rds(conn, cursor, company_uuid):

#     insert_query = """
#     SELECT endpoint_name, role_arn, secret_id, region from companyconnection WHERE company_uuid = %s;
#     """

#     cursor.execute(insert_query, (company_uuid,))
#     rows = cursor.fetchall()

#     return rows[0][0], rows[0][1], rows[0][2], rows[0][3]


def get_parse_response_db(conn, cursor, key):
    x = []
    y = []
    text = []
    query = """
    SELECT return_data
    FROM documenttasks"""+addition+""" 
    WHERE document_uuid = %s
    """

    cursor.execute(query, (key,))
    row = cursor.fetchall()

    
    for r in row[0][0]:
        if r:
            x.append(r["opp_x"])
            y.append(r["opp_y"])
            text.append(r["Text"]),
        else:
            x.append(None)
            y.append(None)
            text.append(None)

    return x, y, text


def get_pastmapped_data(conn, cursor):  
    select_query = """SELECT 
                        CASE
                            WHEN metadata::jsonb ? 'x_dimension' AND metadata::jsonb ? 'y_dimension' THEN CAST(metadata->>'x_dimension' AS FLOAT)
                            ELSE NULL
                        END AS x_dimension,
                        CASE
                            WHEN metadata::jsonb ? 'x_dimension' AND metadata::jsonb ? 'y_dimension' THEN CAST(metadata->>'y_dimension' AS FLOAT)
                            ELSE NULL
                        END AS y_dimension,
                        text
                    FROM 
                        pastperformance"""+addition+""";"""

    cursor.execute(select_query)
    rows = cursor.fetchall()

    x_dimensions = [row[0] for row in rows]
    y_dimensions = [row[1] for row in rows]
    texts = [row[2] for row in rows]

    return x_dimensions, y_dimensions, texts

def get_customer_rds(secret_name, region_name):
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )
    get_secret_value_response = client.get_secret_value(
        SecretId=secret_name
    )
    secret = get_secret_value_response['SecretString']
    values = json.loads(secret)
    return values['endpoint'], values['role_arn'], values['secret_id'], values['region']