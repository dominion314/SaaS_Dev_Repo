import boto3
import base64
import datetime
import os
import logging
import json
from botocore.exceptions import ClientError
from resources.db import Application_RDS_Connection, get_customer_rds

def handler(event, context):
    required_keys = ['process_id', 'company_uuid', 'presigned','keys']
    current_time = datetime.datetime.now()
    try:
        body = json.loads(event['body'])
    except KeyError:
        logging.error(f"No Body")
        return {
            'statusCode': 400,
            'headers': {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            'body': json.dumps({
                'message': f"Missing body in request.",
                'time': str(current_time),
                'id': 'None',
                'file_name': 'none'
            })
        }


    if all(key in body for key in required_keys):
        process_id = body['process_id']
        company_uuid = body['company_uuid']
        presigned = body['presigned']
        keys = body['keys']

        if presigned == 'yes':
            s3_client = boto3.client('s3')
            bucket_name = 'pendingdocuments'
            expiration = 3600 
            urls = []
            try:
                for key in keys:
                    response = s3_client.generate_presigned_url('put_object',
                                                                Params={'Bucket': bucket_name,
                                                                        'Key': key},
                                                                ExpiresIn=expiration)
                    urls.append(response)
            except ClientError as e:
                return {
                    'statusCode': 500,
                    'body': json.dumps({'error': str(e)})
            }
        else:
            urls = []

        try:
            app_conn, app_cursor = Application_RDS_Connection()
            endpoint_name, role_arn, secret_id, region, company_name = get_customer_rds(app_conn, app_cursor, company_uuid)
            

        except ClientError as e:
            logging.error(f"Failed to start ECS Task for ID: {e}")
            return {
                'statusCode': 500,
                'headers': {
                        'Access-Control-Allow-Headers': '*',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f'Failed to start task ECS: {str(e)}',
                    'time': str(current_time),
                    'id': process_id,
                })
            }
        
    else:
        missing_keys = [key for key in required_keys if key not in body]
        return {
                'statusCode': 400,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f"Missing required key(s): {', '.join(missing_keys)}",
                    'time': str(current_time),
                    'id': 'None',
                    'file_name': 'none'
                })
            }

    return {
            'statusCode': 200,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f'Data successfully retrieved',
                    'time': str(current_time),
                    'id': process_id,
                    'endpoint': endpoint_name,
                    'role_arn':role_arn,
                    'secret_id':secret_id,
                    'region':region,
                    'url': urls,
                    'name': company_name
                })
            }