def center_data(data):
    # Calculate the mean of each column
    mean = [sum(column) / len(column) for column in zip(*data)]
    # Subtract the mean from each column
    centered_data = [[value - mean_i for value, mean_i in zip(row, mean)] for row in data]
    return centered_data

def compute_covariance(data):
    n_samples = len(data)
    n_features = len(data[0])
    covariance_matrix = [[0] * n_features for _ in range(n_features)]
    
    for i in range(n_features):
        for j in range(n_features):
            covariance_matrix[i][j] = sum(data[k][i] * data[k][j] for k in range(n_samples)) / (n_samples - 1)
    
    return covariance_matrix

def dot_product(v1, v2):
    return sum(x * y for x, y in zip(v1, v2))

def matrix_multiply(A, B):
    # Transpose B
    B_T = list(zip(*B))
    # Multiply matrices
    return [[dot_product(row, col) for col in B_T] for row in A]

def transpose(matrix):
    return list(zip(*matrix))

def pca(data):
    centered_data = center_data(data)
    covariance_matrix = compute_covariance(centered_data)
    
    # Using a simple eigenvalue, eigenvector solver for symmetric matrices
    # This is a placeholder as computing eigenvectors requires more complex algorithms
    eigenvalues = [covariance_matrix[i][i] for i in range(len(covariance_matrix))]  # Diagonal elements as approximation
    eigenvectors = transpose(covariance_matrix)
    
    # Sort eigenvalues and eigenvectors
    sorted_indices = sorted(range(len(eigenvalues)), key=lambda k: eigenvalues[k], reverse=True)
    eigenvalues = [eigenvalues[i] for i in sorted_indices]
    eigenvectors = [eigenvectors[i] for i in sorted_indices]
    
    # Select the top 2 components
    components = [eigenvectors[i] for i in range(2)]
    
    # Project data
    projected_data = matrix_multiply(centered_data, transpose(components))
    
    x_values = [row[0] for row in projected_data]
    y_values = [row[1] for row in projected_data]
    
    return x_values, y_values
