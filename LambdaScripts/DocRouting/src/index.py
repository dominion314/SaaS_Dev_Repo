import boto3
import base64
import datetime
import os
import logging
import json
from botocore.exceptions import ClientError
from resources.db import add_to_task_db, Customer_RDS_Connection, get_customer_rds

logger = logging.getLogger()
logger.setLevel(logging.INFO)
current_time = datetime.datetime.now()


def handler(event, context):
    required_keys = ['process_id', 'keys', 'ids', 'file_names','process_type', 'secret_name']
    current_time = datetime.datetime.now()
    try:
        body = json.loads(event['body'])
    except KeyError:
        logging.error(f"Failed because there is no body.")
        return {
            'statusCode': 400,
            'headers': {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            'body': json.dumps({
                'message': f"Missing body in request.",
                'time': str(current_time),
                'id': 'None',
                'file_name': 'none'
            })
        }


    if all(key in body for key in required_keys):

        process_id = body['process_id']
        # company_uuid = body['company_uuid']
        keys = body['keys']
        ids = body['ids']
        file_names = body['file_names']
        process = body['process_type']

        secret_name = body['secret_name']
        region_name = os.getenv("SECRET_REGION")
        
        cluster_name = os.getenv("CLUSTER_NAME")
        task_definition = os.getenv("TASK_DEFINITION")
        env_container_name = os.getenv("ENV_CONTAINER_NAME")
        open_ai_key = os.getenv('OPEN_AI_KEY')
        bucket = os.getenv('BUCKET')


        if len(keys) == 0:
            logging.error(f"Failed because keys are empty")
            return {
                'statusCode': 400,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({'error': 'No files provided in the request'})
            }

        try:
            formatted_records = []
            for i in range(len(keys)):
                formatted_record = (
                    ids[i],
                    file_names[i],
                    process,
                    str(current_time),
                    None,
                    f"{ids[i]}_{file_names[i]}",
                    False,
                    'processing',
                    None,
                    None
                    )
                formatted_records.append(formatted_record)
            
            endpoint_name, role_arn, secret_id, region = get_customer_rds(secret_name, region_name)
            cus_conn, cus_cursor = Customer_RDS_Connection(endpoint_name, role_arn, secret_id, region)
            add_to_task_db(cus_conn, cus_cursor, formatted_records)

        except Exception as e:
            s3_client = boto3.client('s3')
            for key in keys:
                s3_client.delete_object(Bucket=bucket, Key=key)
            logging.error(f"Failed to add task to database and files are deleted: {e}")
            return {
                'statusCode': 500,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f'Failed to add task to database and files are deleted: {e}',
                    'time': str(current_time),
                    'id': process_id,
                })
            }
        
        try:
            ecs = boto3.client('ecs')
            response = ecs.run_task(
                        cluster=cluster_name,
                        launchType='FARGATE',
                        taskDefinition=task_definition,
                        networkConfiguration={
                            'awsvpcConfiguration': {
                                'subnets': ['subnet-0c61ce459f04c4cab'],  
                                'securityGroups': ['sg-0397a41cf2dce79c4'],  
                                'assignPublicIp': 'DISABLED' 
                            }
                        },

                        overrides={
                            'containerOverrides': [
                                {
                                    'name': env_container_name,
                                    'environment': [
                                        {'name':'S3_BUCKET', 'value': bucket},
                                        {'name':'S3_KEY', 'value': str(keys)},
                                        {'name':'PROCESS_ID','value': process_id},
                                        {'name':'PROCESS_TYPE','value': process},
                                        {'name':'SECRET_NAME','value': os.getenv("SECRET_NAME")},
                                        {'name':'SECRET_REGION','value': os.getenv("SECRET_REGION")},
                                        {'name':'DB_HOST','value': os.getenv("DB_HOST")},
                                        # {'name':'COMPANY_UUID','value': company_uuid},
                                        {'name':'AWS_ACCESS_KEY_ID_SAGEMAKER','value': os.getenv("AWS_ACCESS_KEY_ID_SAGEMAKER")},
                                        {'name':'AWS_SECRET_ACCESS_KEY_SAGEMAKER','value': os.getenv("AWS_SECRET_ACCESS_KEY_SAGEMAKER")},
                                        {'name':'REGION_NAME_SAGEMAKER','value': os.getenv("REGION_NAME_SAGEMAKER")},
                                        {'name':'ENDPOINT_NAME_SAGEMAKER','value': os.getenv("ENDPOINT_NAME_SAGEMAKER")},
                                        {'name':'CUSTOMER_ENDPOINT_NAME','value': endpoint_name},
                                        {'name':'CUSTOMER_ROLE_ARN','value': role_arn},
                                        {'name':'CUSTOMER_SECRET_ID','value': secret_id},
                                        {'name':'CUSTOMER_REGION','value': region},
                                        {'name':'OPEN_AI_KEY','value': open_ai_key},
                                        {'name':'ENV_NAME','value': os.getenv("ENV")},
                                    ]
                                }
                            ]
                        },
                    )

        except ClientError as e:
            logging.error(f"Failed to start ECS task: {e}")
            formatted_records = []
            for i in range(len(keys)):
                formatted_record = (
                    ids[i],
                    file_names[i],
                    process,
                    str(current_time),
                    None,
                    f"{ids[i]}_{file_names[i]}",
                    False,
                    'failed',
                    None,
                    None
                    )
                formatted_records.append(formatted_record)

            cus_conn, cus_cursor = Customer_RDS_Connection(endpoint_name, role_arn, secret_id, region)
            add_to_task_db(cus_conn, cus_cursor, formatted_records)
            return {
                'statusCode': 500,
                'headers': {
                        'Access-Control-Allow-Headers': '*',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f'Failed to start task ECS: {str(e)}',
                    'time': str(current_time),
                    'id': process_id,
                })
            }

    else:
        missing_keys = [key for key in required_keys if key not in body]
        return {
                'statusCode': 400,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f"Missing required key(s): {', '.join(missing_keys)}",
                    'time': str(current_time),
                    'id': 'None',
                    'file_name': 'none'
                })
            }

    return {
            'statusCode': 200,
                'headers': {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
                },
                'body': json.dumps({
                    'message': f'Process started successfully.',
                    'time': str(current_time),
                    'id': process_id,
                })
            }