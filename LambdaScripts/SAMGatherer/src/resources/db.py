import os
import pg8000
import ssl
import boto3
from botocore.exceptions import ClientError
import json


def get_secret():
    secret_name = os.getenv("SECRET_NAME")
    region_name = os.getenv("SECRET_REGION")

    client = boto3.client('secretsmanager', region_name=region_name)

    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    except ClientError as e:
        raise e
        
    secret = json.loads(get_secret_value_response['SecretString'])
    return secret

def RDS_Connection():
    secret = get_secret()
    
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_NONE
    
    host = os.getenv("DB_HOST")
    port = 5432 
    dbname = secret['username']
    user = secret['username']
    password = secret['password']

    conn = pg8000.connect(
        host=host,
        port=port,
        database=dbname,
        user=user,
        password=password,
        ssl_context=ssl_context)
    
    cursor = conn.cursor()
    table_name = "samdata"
    cursor.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename = %s)", (table_name,))
    exists = cursor.fetchone()[0]

    if not exists:
        create_table_query = """
        CREATE TABLE samdata (
            id SERIAL PRIMARY KEY,
            opportunity_id TEXT,
            posted_date TEXT,
            title TEXT,
            type TEXT,
            response_deadline TEXT,
            ui_link TEXT,
            document TEXT,
            vector_all_MiniLM_L6_v2 vector
        );
        """
        cursor.execute(create_table_query)
        conn.commit()

    return conn, cursor


def add_to_RDS(metadatas, ids, documents, vectors):
    conn, cursor = RDS_Connection()
    for i in range(len(metadatas)):
        insert_query = """
        INSERT INTO samdata (opportunity_id, posted_date, title, type, response_deadline, ui_link, document, vector_all_MiniLM_L6_v2)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
        """
        metadata = metadatas[i]
        doc = documents[i]
        vec = str(vectors[i])
        opp_id = ids[i]

        cursor.execute(insert_query, (opp_id, metadata['postedDate'], metadata['title'], metadata['type'], metadata['responseDeadLine'], metadata['uiLink'], doc, vec))

    conn.commit()
    cursor.close()
    conn.close()