import os
import boto3
import json

def vectorize(documents):
    id = os.getenv("AWS_ACCESS_KEY_ID_SAGEMAKER")
    secret_key = os.getenv("AWS_SECRET_ACCESS_KEY_SAGEMAKER")
    region = os.getenv("REGION_NAME_SAGEMAKER")
    endpoint_name = os.getenv("ENDPOINT_NAME_SAGEMAKER")

    client = boto3.client('sagemaker-runtime',
                      aws_access_key_id=id,
                      aws_secret_access_key=secret_key,
                      region_name=region)

    input_json = json.dumps(documents)
    input_bytes = input_json.encode('utf-8')
    response = client.invoke_endpoint(
        EndpointName=endpoint_name,
        ContentType='application/json',
        Body=input_bytes)

    vectors = json.loads(response['Body'].read())
    return vectors