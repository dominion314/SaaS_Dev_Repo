import urllib3
import json
import re

def clean_up_text(text):
    html_tags_pattern = re.compile(r'<[^>]+>')
    text = html_tags_pattern.sub('', text)
    text = text.replace('\n',' ')
    text = text.replace('\t',' ')
    text = text.replace('&nbsp;',' ')
    text = text.replace('  ',' ')
    text = text.replace('  ',' ')
    return text

def sam_description_compiler(api_response_content, api_key):
    descriptions = []
    for x in api_response_content['opportunitiesData']:
        url = x['description']+f"&api_key={api_key}"

        http = urllib3.PoolManager()
        response = http.request('GET', url)
        if response.status == 200:
            description = json.loads(response.data.decode('utf-8'))['description']
            description = clean_up_text(description)
            descriptions.append(description)

        else:
            descriptions.append('')
    return descriptions

def fix_none_dict(input_list):
    modified_list = []
    for my_dict in input_list:
        modified_dict = {}
        for key, value in my_dict.items():
            modified_dict[key] = 'unknown' if value is None else value
        modified_list.append(modified_dict)
    return modified_list

def process_SAM_response(api_response_content, api_key):
    ids = [x['noticeId'] for x in api_response_content['opportunitiesData']]
    metadatas = [
        {
            'postedDate': x.get('postedDate', 'Unknown'),
            'title': x.get('title', 'Unknown'),
            'type': x.get('type', 'Unknown'),
            'responseDeadLine': x.get('responseDeadLine', 'Unknown'),
            'uiLink': x.get('uiLink', 'Unknown')
        } for x in api_response_content['opportunitiesData']]
    metadatas = fix_none_dict(metadatas)
    documents = sam_description_compiler(api_response_content, api_key)
    return ids, metadatas, documents