import urllib3
import json
from datetime import date, timedelta
import os
import logging
from resources.db import add_to_RDS
from resources.samfunctions import process_SAM_response
from resources.sagemaker import vectorize

def handler(event, context):
    api_keys = [os.getenv("SAM_API_KEY"), os.getenv("SAM_API_KEY2")]
    today = date.today()
    yesterday = today - timedelta(days=2)
    today_str = today.strftime("%m/%d/%Y")
    yesterday_str = yesterday.strftime("%m/%d/%Y")

    for api_key in api_keys:
        url = f"https://api.sam.gov/prod/opportunities/v2/search?limit=1000&api_key={api_key}&postedFrom={yesterday_str}&postedTo={today_str}&ptype=r&ptype=p"

        http = urllib3.PoolManager()
        response = http.request('GET', url)
        
        if response.status == 200:
            opportunities = json.loads(response.data.decode('utf-8'))
            ids, metadatas, documents = process_SAM_response(opportunities, api_key)
            break
        elif response.status == 429:
            logging.warning(f"Rate limit exceeded for API key: {api_key}. Retrying with the next API key.")
        else:
            logging.error(f"HTTP request failed with status code: {response.status}")
            return { 
                'messages' : str(response.status)
            }, 500
        
    try:
        vectors = vectorize(documents)
        
    except Exception as e:
        logging.error(f"Vectorization Process Failed due to: {str(e)}")
        return { 
                'message' : f'Vectorization Process Failed due to {str(e)}'
                }, 500
                
    try:
        add_to_RDS(metadatas, ids, documents, vectors)

    except Exception as e:
        logging.error(f'Database entry Process Failed due to: {str(e)}')
        return { 
                'message' : f'Database entry Process Failed due to {str(e)}'
                }, 500
    
    logging.info(f'Successfully updated SAMData for {today_str}')    
    return { 
        'message' :'successfully updated'
            }, 200