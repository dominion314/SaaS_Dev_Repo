from flask import Flask, render_template, request
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import requests
from googletrans import Translator
import time


app = Flask(__name__)
translator = Translator()

# Function to translate text to English
def translate_to_english(text):
    translated = translator.translate(text, dest='en')
    return translated.text

# Function to scrape the Studio Marmo website for data
# Function to scrape the Studio Marmo website for data
# Function to scrape the Studio Marmo website for data

def get_stone_data():
    url = "http://www.studiomarmo.it/stone_cloud.php"

    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36"
    }

    # Get the page source using requests and parse it with BeautifulSoup
    response = requests.get(url, headers=headers)
    soup = BeautifulSoup(response.text, "html.parser")

    # Scrape dropdown data
    type_select = soup.find('select', {'id': 'type'})
    color_select = soup.find('select', {'id': 'color'})
    origin_select = soup.find('select', {'id': 'origin'})

    types = [option.text for option in type_select.find_all('option')] if type_select else []
    colors = [option.text for option in color_select.find_all('option')] if color_select else []
    origins = [option.text for option in origin_select.find_all('option')] if origin_select else []

    # Scrape the stones data based on the website structure
    stones = []
    for stone_selector in soup.find_all("div", class_="stone_selector"):
        stone_data = {
            "name": stone_selector.find("div", class_="name").text,
            "image_url": "http://www.studiomarmo.it/" + stone_selector.find("img")["src"],
            "description": stone_selector.find("div", class_="description").text,
        }
        stones.append(stone_data)

    return stones, types, colors, origins

@app.route("/", methods=["GET", "POST"])
def index():
    stones, types, colors, origins = get_stone_data()
    query = ""

    if request.method == "POST":
        query = request.form["query"]
        stones = [stone for stone in stones if query.lower() in stone["name"].lower()]

    return render_template("index.html", stones=stones, query=query, types=types, colors=colors, origins=origins)

if __name__ == "__main__":
    app.run(debug=True)
