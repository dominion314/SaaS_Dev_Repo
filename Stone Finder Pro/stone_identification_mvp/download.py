import os
import requests
from bs4 import BeautifulSoup

def download_file(url, local_filename):
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192):
                f.write(chunk)

def download_stone_data():
    url = "http://www.studiomarmo.it/stone_cloud.php"
    base_url = "http://www.studiomarmo.it/"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")

    # Create a directory to store the downloaded files
    os.makedirs("stone_cloud_data", exist_ok=True)

    # Download text content
    with open("stone_cloud_data/stone_cloud_text.txt", "w") as f:
        f.write(soup.get_text())

    # Download images
    for stone_selector in soup.find_all("div", class_="stone_selector"):
        image_url = base_url + stone_selector.find("img")["src"]
        image_name = image_url.split("/")[-1]
        local_filename = os.path.join("stone_cloud_data", image_name)
        download_file(image_url, local_filename)

download_stone_data()
