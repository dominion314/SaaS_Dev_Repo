Based on my research, I have developed the following sales projection for the next year:

Quarter 1:

Launch: We will focus on marketing, promotion, and user acquisition during this period.
Expected number of users: 500 (primarily early adopters and users from our existing network)
Revenue: $2,500 (assuming an average subscription fee of $5 per user)

Quarter 2:

Growth: We will expand our marketing efforts and refine the app based on user feedback.
Expected number of users: 2,500 (as our marketing efforts gain traction and the app gains visibility)
Revenue: $12,500 (assuming an average subscription fee of $5 per user)

Quarter 3:

Scaling: We will continue to scale our marketing efforts and introduce new features.
Expected number of users: 6,000 (further expansion of our user base through marketing and word-of-mouth)
Revenue: $30,000 (assuming an average subscription fee of $5 per user)

Quarter 4:

Maturity: We will have a stable user base and continue to optimize the app for retention and user satisfaction.
Expected number of users: 10,000 (as our app establishes itself in the market)
Revenue: $50,000 (assuming an average subscription fee of $5 per user)
Total annual revenue projection: $95,000

Please note that these projections are based on a number of assumptions, such as marketing effectiveness, user acquisition, and average subscription fees.