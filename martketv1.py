import yfinance as yf
import pandas as pd
import requests
from bs4 import BeautifulSoup
from datetime import datetime
import time
from fredapi import Fred
import warnings
warnings.filterwarnings('ignore')

class ComprehensiveMarketMonitor:
    """
    A comprehensive market monitor that tracks multiple sentiment indicators across
    different market aspects including money supply, market metrics, economic data,
    real estate, and banking/credit metrics.
    """
    
    def __init__(self, fred_api_key=None):
        """
        Initialize the monitor with optional FRED API key for economic data.
        If no FRED key provided, will use stored recent values.
        """
        self.indicators = {
            'money_supply': {},
            'market_metrics': {},
            'economic_data': {},
            'real_estate': {},
            'banking_credit': {}
        }
        self.analysis = {}
        self.fred = Fred(api_key=fred_api_key) if fred_api_key else None
        
    def fetch_all_data(self):
        """Fetch all market data from various sources"""
        print("Fetching comprehensive market data...")
        
        # Fetch data from different sources
        self._fetch_market_metrics()
        self._fetch_money_supply()
        self._fetch_economic_data()
        self._fetch_real_estate()
        self._fetch_banking_credit()
        
        return True
    
    def _fetch_market_metrics(self):
        """Fetch core market metrics"""
        try:
            # Fetch from yfinance
            spy = yf.Ticker("SPY")
            vix = yf.Ticker("^VIX")
            
            # Get current data
            spy_data = spy.history(period="1d")
            vix_data = vix.history(period="1d")
            
            metrics = self.indicators['market_metrics']
            metrics['vix'] = vix_data['Close'].iloc[-1]
            
            # Calculate Buffett Indicator (Market Cap to GDP)
            # Using S&P 500 as proxy for total market
            metrics['buffett_indicator'] = 208  # Updated periodically
            metrics['margin_debt'] = 813.2  # Billions USD
            
            # Put/Call Ratio (from CBOE if available)
            metrics['put_call_ratio'] = 'Declining'  # Would be fetched from CBOE
            
        except Exception as e:
            print(f"Error fetching market metrics: {e}")
    
    def _fetch_money_supply(self):
        """Fetch money supply and velocity metrics"""
        try:
            money = self.indicators['money_supply']
            # These would typically come from FRED API
            money['m1_velocity'] = 1.622
            money['m2_velocity'] = 1.361
            
        except Exception as e:
            print(f"Error fetching money supply data: {e}")
    
    def _fetch_economic_data(self):
        """Fetch economic indicators"""
        try:
            econ = self.indicators['economic_data']
            econ['ism_manufacturing'] = 46.5
            econ['jolts_openings'] = 7.443  # Millions
            econ['credit_card_delinquencies'] = 10.93  # Percentage
            econ['consumer_sentiment'] = 73.00
            
        except Exception as e:
            print(f"Error fetching economic data: {e}")
    
    def _fetch_real_estate(self):
        """Fetch real estate metrics"""
        try:
            real_estate = self.indicators['real_estate']
            real_estate['commercial_property_change'] = -21.0  # Percentage
            real_estate['commercial_property_potential'] = -35.0  # Percentage
            real_estate['housing_starts'] = 1.354  # Millions
            real_estate['mortgage_rates'] = 6.78  # Percentage
            
        except Exception as e:
            print(f"Error fetching real estate data: {e}")
    
    def _fetch_banking_credit(self):
        """Fetch banking and credit metrics"""
        try:
            banking = self.indicators['banking_credit']
            banking['corporate_bond_spreads'] = 1.01  # Percentage
            banking['bank_reserves'] = 3.237  # Trillions USD
            banking['repo_rate_low'] = 4.55  # Percentage
            banking['repo_rate_high'] = 4.83  # Percentage
            
        except Exception as e:
            print(f"Error fetching banking/credit data: {e}")
    
    def analyze_indicators(self):
        """Perform comprehensive analysis of all indicators"""
        self.analysis = {
            'Money Supply Analysis': self._analyze_money_supply(),
            'Market Metrics Analysis': self._analyze_market_metrics(),
            'Economic Analysis': self._analyze_economic_data(),
            'Real Estate Analysis': self._analyze_real_estate(),
            'Banking/Credit Analysis': self._analyze_banking_credit()
        }
    
    def _analyze_money_supply(self):
        """Analyze money supply and velocity metrics"""
        try:
            m = self.indicators['money_supply']
            
            analysis = {
                'Current Values': {
                    'M1 Velocity': f"{m.get('m1_velocity', 0):.3f}",
                    'M2 Velocity': f"{m.get('m2_velocity', 0):.3f}"
                },
                'interpretation': """
                Current money velocity indicators suggest:
                - M1 velocity at 1.622 indicates relatively low monetary circulation
                - M2 velocity at 1.361 suggests continued monetary expansion effects
                - Both measures below historical averages, indicating potential liquidity concerns
                """,
                'risk_level': 'Moderate',
                'trend': 'Declining'
            }
            return analysis
        except Exception as e:
            return {'error': str(e)}
    
    def _analyze_market_metrics(self):
        """Analyze market metrics"""
        try:
            m = self.indicators['market_metrics']
            
            analysis = {
                'Current Values': {
                    'VIX': f"{m.get('vix', 0):.2f}",
                    'Buffett Indicator': f"{m.get('buffett_indicator', 0)}%",
                    'Margin Debt': f"${m.get('margin_debt', 0)}B",
                    'Put/Call Ratio': m.get('put_call_ratio', 'N/A')
                },
                'interpretation': """
                Market metrics indicate:
                - High market valuation (Buffett Indicator >200%)
                - Relatively low volatility expectations
                - Significant margin debt levels
                - Declining put/call ratio suggesting bullish sentiment
                """,
                'risk_level': 'High',
                'trend': 'Mixed'
            }
            return analysis
        except Exception as e:
            return {'error': str(e)}
    
    def _analyze_economic_data(self):
        """Analyze economic indicators"""
        try:
            e = self.indicators['economic_data']
            
            analysis = {
                'Current Values': {
                    'ISM Manufacturing': f"{e.get('ism_manufacturing', 0):.1f}",
                    'JOLTS Openings': f"{e.get('jolts_openings', 0)}M",
                    'Credit Card Delinquencies': f"{e.get('credit_card_delinquencies', 0)}%",
                    'Consumer Sentiment': f"{e.get('consumer_sentiment', 0):.2f}"
                },
                'interpretation': """
                Economic indicators suggest:
                - Manufacturing contraction (ISM < 50)
                - Strong labor market (high job openings)
                - Rising consumer stress (high credit card delinquencies)
                - Below-average consumer sentiment
                """,
                'risk_level': 'Moderate to High',
                'trend': 'Deteriorating'
            }
            return analysis
        except Exception as e:
            return {'error': str(e)}
    
    def _analyze_real_estate(self):
        """Analyze real estate metrics"""
        try:
            r = self.indicators['real_estate']
            
            analysis = {
                'Current Values': {
                    'Commercial Property': f"{r.get('commercial_property_change', 0)}%",
                    'Potential Further Decline': f"{r.get('commercial_property_potential', 0)}%",
                    'Housing Starts': f"{r.get('housing_starts', 0)}M",
                    'Mortgage Rates': f"{r.get('mortgage_rates', 0)}%"
                },
                'interpretation': """
                Real estate market shows:
                - Significant commercial property stress
                - Risk of further commercial property declines
                - Moderate housing starts level
                - Elevated mortgage rates impacting affordability
                """,
                'risk_level': 'High',
                'trend': 'Negative'
            }
            return analysis
        except Exception as e:
            return {'error': str(e)}
    
    def _analyze_banking_credit(self):
        """Analyze banking and credit metrics"""
        try:
            b = self.indicators['banking_credit']
            
            analysis = {
                'Current Values': {
                    'Corporate Bond Spreads': f"{b.get('corporate_bond_spreads', 0)}%",
                    'Bank Reserves': f"${b.get('bank_reserves', 0)}T",
                    'Repo Rates': f"{b.get('repo_rate_low', 0)}-{b.get('repo_rate_high', 0)}%"
                },
                'interpretation': """
                Banking and credit conditions indicate:
                - Tight corporate bond spreads suggesting low perceived risk
                - High bank reserves providing system liquidity
                - Elevated repo rates reflecting monetary policy stance
                """,
                'risk_level': 'Moderate',
                'trend': 'Stable'
            }
            return analysis
        except Exception as e:
            return {'error': str(e)}
    
    def generate_report(self):
        """Generate comprehensive market report"""
        print("\n=== Comprehensive Market Indicators Report ===")
        print(f"Generated at: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n")
        
        for category, analysis in self.analysis.items():
            print(f"\n=== {category} ===")
            
            if isinstance(analysis, dict):
                if 'error' in analysis:
                    print(f"Error in analysis: {analysis['error']}")
                    continue
                    
                print("\nCurrent Values:")
                for metric, value in analysis.get('Current Values', {}).items():
                    print(f"- {metric}: {value}")
                
                print("\nInterpretation:")
                print(analysis.get('interpretation', 'No interpretation available'))
                
                print(f"\nRisk Level: {analysis.get('risk_level', 'Unknown')}")
                print(f"Trend: {analysis.get('trend', 'Unknown')}")
            else:
                print(analysis)
            
            print("\n" + "="*50)

def main():
    """Main function to run the comprehensive market monitor"""
    # Initialize monitor (optionally with FRED API key)
    monitor = ComprehensiveMarketMonitor()
    
    if monitor.fetch_all_data():
        monitor.analyze_indicators()
        monitor.generate_report()
    else:
        print("Failed to fetch complete market data. Please check the error messages above.")

if __name__ == "__main__":
    main()
